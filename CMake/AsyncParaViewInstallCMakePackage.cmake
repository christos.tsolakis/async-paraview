if (NOT (DEFINED apv_cmake_dir AND
         DEFINED apv_cmake_build_dir AND
         DEFINED apv_cmake_destination AND
         DEFINED apv_modules))
  message(FATAL_ERROR
    "AsyncParaViewInstallCMakePackage is missing input variables.")
endif ()

_vtk_module_write_import_prefix("${apv_cmake_build_dir}/apv-prefix.cmake" "${apv_cmake_destination}")

string(REPLACE "AsyncParaView::" "" _paraview_all_components "${apv_modules};${apv_client_modules}")

configure_file(
  "${apv_cmake_dir}/apv-config.cmake.in"
  "${apv_cmake_build_dir}/apv-config.cmake"
  @ONLY)
write_basic_package_version_file("${apv_cmake_build_dir}/apv-config-version.cmake"
  VERSION "${APV_VERSION_FULL}"
  COMPATIBILITY AnyNewerVersion)

# For convenience, a package is written to the top of the build tree. At some
# point, this should probably be deprecated and warn when it is used.
file(GENERATE
  OUTPUT  "${CMAKE_BINARY_DIR}/apv-config.cmake"
  CONTENT "include(\"${apv_cmake_build_dir}/apv-config.cmake\")\n")
configure_file(
  "${apv_cmake_build_dir}/apv-config-version.cmake"
  "${CMAKE_BINARY_DIR}/apv-config-version.cmake"
  COPYONLY)

set(paraview_cmake_module_files
  # Client API
  paraview_servermanager_convert_categoryindex.xsl
  paraview_servermanager_convert_html.xsl
  paraview_servermanager_convert_wiki.xsl.in
  paraview_servermanager_convert_xml.xsl

  # ServerManager API
  AsyncParaViewServerManager.cmake

  # Testing
  AsyncParaViewTesting.cmake

  # Client Server
  #vtkModuleWrapClientServer.cmake
  )

set(apv_cmake_files_to_install)
foreach (paraview_cmake_module_file IN LISTS paraview_cmake_module_files)
  configure_file(
    "${apv_cmake_dir}/${paraview_cmake_module_file}"
    "${apv_cmake_build_dir}/${paraview_cmake_module_file}"
    COPYONLY)
  list(APPEND apv_cmake_files_to_install
    "${paraview_cmake_module_file}")
endforeach ()

if (NOT APV_RELOCATABLE_INSTALL)
  list(APPEND apv_cmake_files_to_install
    "${apv_cmake_build_dir}/paraview-find-package-helpers.cmake")
endif ()

foreach (apv_cmake_file IN LISTS apv_cmake_files_to_install)
  if (IS_ABSOLUTE "${apv_cmake_file}")
    file(RELATIVE_PATH paraview_cmake_subdir_root "${apv_cmake_build_dir}" "${apv_cmake_file}")
    get_filename_component(paraview_cmake_subdir "${paraview_cmake_subdir_root}" DIRECTORY)
    set(paraview_cmake_original_file "${apv_cmake_file}")
  else ()
    get_filename_component(paraview_cmake_subdir "${apv_cmake_file}" DIRECTORY)
    set(paraview_cmake_original_file "${apv_cmake_dir}/${apv_cmake_file}")
  endif ()
  install(
    FILES       "${paraview_cmake_original_file}"
    DESTINATION "${apv_cmake_destination}/${paraview_cmake_subdir}"
    COMPONENT   "development")
endforeach ()

install(
  FILES       "${apv_cmake_build_dir}/apv-config.cmake"
              "${apv_cmake_build_dir}/apv-config-version.cmake"
              "${apv_cmake_build_dir}/apv-prefix.cmake"
  DESTINATION "${apv_cmake_destination}"
  COMPONENT   "development")

vtk_module_export_find_packages(
  CMAKE_DESTINATION "${apv_cmake_destination}"
  FILE_NAME         "ParaView-vtk-module-find-packages.cmake"
  MODULES           ${apv_modules})
