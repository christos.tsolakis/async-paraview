Contributing to ParaView Async
===============================

This page documents at a very high level how to contribute to ParaView.
Please check our [developer instructions][] for a more detailed guide to
developing and contributing to the project, and our [ParaView Async Git README][]
for additional information.

The ParaView development cycle is built upon the following components:

1. [Issues][] identify any issues including bugs and feature requests. In
   general, every code change should have an associated issue which identifies
   the bug being fixed or the feature being added.

2. [Merge Requests][] are collections of changes that address issues.

Reporting Issues
================

If you have a bug report or a feature request for ParaView, you can use the
[issues][] tracker to report a [new issue][].

To report an issue.

1. Register [GitLab Access] to create an account and select a user name.
2. Create a [new issue][].
3. Ensure that the  issue has a **Title** and **Description**
   with enough details for someone from the development team to reproduce the
   issue. See [Gitlab Markdown] guide for styling the **Description**. Include
   screenshots and sample datasets whenever possible.

Fixing issues
=============

Typically, one addresses issues by writing code. To start contributing to ParaView Async:

1.  Register [GitLab Access] to create an account and select a user name.

2.  [Fork ParaView Async][] into your user's namespace on GitLab.

3.  Create a local clone of the main ParaView repository. Optionally configure
    Git to [use SSH instead of HTTPS][].
    Then clone:

        $ git clone --recursive https://gitlab.kitware.com/async/paraview.git ParaViewAsync
        $ cd ParaViewAsync
    The main repository will be configured as your `origin` remote.

    For more information see: [Setup][] and [download instructions][]

4.  Run the [developer setup script][] to prepare your ParaViewAsync work
    tree and create Git command aliases used below:

        $ ./Utilities/SetupForDevelopment.sh
    This will prompt for your GitLab user name and configure a remote
    called `gitlab` to refer to it. Choose the defaults for ParaView Data questions.

    For more information see: [Setup][]

5.  [Build Paraview Async][] and run it.

6.  Edit files and create commits (repeat as needed):

        $ edit file1 file2 file3
        $ git add file1 file2 file3
        $ git commit

    Commit messages must be thorough and informative so that
    reviewers will have a good understanding of why the change is
    needed before looking at the code. Appropriately refer to the issue
    number, if applicable.

    For more information see: [Create a Topic][]

7.  Push commits in your topic branch to your fork in GitLab:

        $ git gitlab-push

    For more information see: [Share a Topic][]

8.  Run tests with ctest, or use the dashboard

9.  Visit your fork in GitLab, browse to the "**Merge Requests**" link on the
    left, and use the "**New Merge Request**" button in the upper right to
    create a Merge Request.

    For more information see: [Create a Merge Request][]

8.  Follow the [review][] process to get your merge request reviewed and tested.
    On success, the merge-request can be merged and closed.

    For more information see: [Review a Merge Request][]

9.  When a merge request is closed, any related issue should be closed (if not
    closed automatically).


[ParaView Async Git README]: Documentation/dev/git/README.md
[developer instructions]: Documentation/dev/git/develop.md
[GitLab Access]: https://gitlab.kitware.com/users/sign_in
[Fork ParaView Async]: https://gitlab.kitware.com/async/paraview/-/forks/new
[use SSH instead of HTTPS]: Documentation/dev/git/download.md#use-ssh-instead-of-https
[download instructions]: Documentation/dev/git/download.md#clone
[developer setup script]: /Utilities/SetupForDevelopment.sh
[Setup]: Documentation/dev/git/develop.md#Setup
[Build Paraview Async]: Documentation/dev/build.md
[Create a Topic]: Documentation/dev/git/develop.md#create-a-topic
[Share a Topic]: Documentation/dev/git/develop.md#share-a-topic
[Create a Merge Request]: Documentation/dev/git/develop.md#create-a-merge-request
[Review a Merge Request]: Documentation/dev/git/develop.md#review-a-merge-request
[review]: Documentation/dev/git/develop.md#review-a-merge-request
[Issues]: https://gitlab.kitware.com/async/paraview/-/issues
[Merge Requests]: https://gitlab.kitware.com/async/paraview/-/merge_requests
[Gitlab Markdown]: https://gitlab.kitware.com/help/markdown/markdown
[new issue]: https://gitlab.kitware.com/async/paraview/-/issues/new
