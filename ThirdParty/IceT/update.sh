#!/usr/bin/env bash

set -e
set -x
shopt -s dotglob

readonly name="icet"
readonly ownership="IceT Upstream <kwrobot@kitware.com>"
readonly subtree="ThirdParty/IceT/vtk$name"
readonly repo="https://gitlab.kitware.com/paraview/icet.git"
readonly tag="for/paraview"
readonly paths="
cmake
src
CMakeLists.txt
README.md
"

extract_source () {
    git_archive
    pushd "$extractdir"
    #TODO move thes changes to thirdparty repo
    sed -i "s/ParaView/AsyncParaView/g" "$name-reduced/CMakeLists.txt"
    # fix whitespace issues
    sed -i 's/\t/  /g' "$name-reduced/src/ice-t/decompress_template_body.h"
    sed -i 's/\t/  /g' "$name-reduced/src/strategies/sequential.c"
    sed -i '$ d' "$name-reduced/cmake/IceTConfig.cmake.in"
    sed -i 's/\t/  /g' "$name-reduced/src/include/IceTDevDiagnostics.h"
    # trailing whitespace fix
    sed -i 's/[\t ]*$//g' "$name-reduced/src/strategies/common.h"
    sed -i 's/[\t ]*$//g' "$name-reduced/src/strategies/reduce.c"
    sed -i 's/[\t ]*$//g' "$name-reduced/src/strategies/vtree.c"

    popd
}

. "${BASH_SOURCE%/*}/../update-common.sh"
