#!/bin/sh

set -e

readonly spackroot="$CI_PROJECT_DIR/.gitlab/spack"

# Clone Spack itself.
git clone --depth 1 https://github.com/spack/spack "$spackroot/src"
export SPACK_ROOT="$spackroot/src"
export PATH="$PATH:$SPACK_ROOT/bin"

# Search for the compiler.
spack compiler find

# Clone mochi package repository.
git clone --depth 1 https://github.com/mochi-hpc/mochi-spack-packages "$spackroot/mochi"
cd "$spackroot/mochi"
git apply "$CI_PROJECT_DIR/.gitlab/ci/patches/mochi/0001-Override-when-attribute-of-argobots-dependencies.patch"
cd ..

# Add mochi repo to Spack.
spack repo add "$spackroot/mochi"

# Inject CI tools to avoid builds.
mkdir -p "$spackroot/config"
case "$CMAKE_CONFIGURATION" in
  fedora*)
    cat > "$spackroot/config/packages.yaml" <<EOF
packages:
  all:
    target: [x86_64]
  autoconf:
    buildable: false
    externals:
    - spec: autoconf@$( rpm -q --qf '%{version}' autoconf )
      prefix: /usr
  automake:
    buildable: false
    externals:
    - spec: automake@$( rpm -q --qf '%{version}' automake )
      prefix: /usr
  boost:
    buildable: false
    externals:
    - spec: boost@$( rpm -q --qf '%{version}' boost-devel )
      prefix: /usr
  cereal:
    buildable: false
    externals:
    - spec: cereal@$( rpm -q --qf '%{version}' cereal-devel )
      prefix: /usr
  cmake:
    buildable: false
    externals:
    - spec: cmake@$( cmake --version | head -n1 | cut -d' ' -f3 )
      prefix: $CI_PROJECT_DIR/.gitlab/cmake
  libtool:
    buildable: false
    externals:
    - spec: libtool@$( rpm -q --qf '%{version}' libtool )
      prefix: /usr
  libsigsegv:
    buildable: false
    externals:
    - spec: libsigsegv@$( rpm -q --qf '%{version}' libsigsegv-devel )
      prefix: /usr
  m4:
    buildable: false
    externals:
    - spec: m4@$( rpm -q --qf '%{version}' m4 )
      prefix: /usr
  perl:
    buildable: false
    externals:
    - spec: perl@$( rpm -q --qf '%{version}' perl )
      prefix: /usr
  pkgconf:
    buildable: false
    externals:
    - spec: pkgconf@$( rpm -q --qf '%{version}' pkgconf )
      prefix: /usr
EOF
    ;;

    macos*)
    cat > "$spackroot/config/packages.yaml" <<EOF
packages:
  all:
    target: [$( uname -m )]
  cmake:
    buildable: false
    externals:
    - spec: cmake@$( cmake --version | head -n1 | cut -d' ' -f3 )
      prefix: $CI_PROJECT_DIR/.gitlab/cmake
EOF
    ;;
  *)
    echo "error: unrecognized configuration: '$CMAKE_CONFIGURATION'"
    exit 1
esac

readonly spec="mochi-thallium@0.10.1 ^libfabric fabrics=tcp,rxm,sockets ^mochi-margo@0.9.10 ^mercury@2.2.0 ^argobots@dce6e727ffc4ca5b3ffc04cb9517c6689be51ec5=main"
# inspect spec
spack -C "$spackroot/config" spec $spec

# Install thallium.
spack -C "$spackroot/config" install $spec

# Clean up the directory.
spack -C "$spackroot/config" clean
