set(test_exclusions)

list(APPEND test_exclusions
  # currently broken on the ci
  "^pv.TestDevelopmentInstall$"
  )

if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "asan" OR 
    "$ENV{CMAKE_CONFIGURATION}" MATCHES "tsan" OR
    "$ENV{CMAKE_CONFIGURATION}" MATCHES "ubsan")

    # disable rendering tests on sanitizers since the overhead added in opengl
    # context  operations make the two-stage rendering of viewproxy.Update() unreliable.
    list(APPEND test_exclusions
    "^AsyncParaView::APVPythonPython-TestPipelineBuilderWithRendering$"
    "^AsyncParaView::APVPythonPython-TestWaveletContourPipelineWithRendering$"
    "^AsyncParaView::APVPythonPython-TestRenderViewStreams$"
    "^AsyncParaView::RemotingServerManagerViewsCxx-TestBasicRenderView$"
    "^AsyncParaView::RemotingMicroservicesCxx-TestPipelineBuilderMicroserviceWithRendering$"
    "^pvcs.TestPipelineBuilderMicroserviceWithRendering$"
    )

endif()

string(REPLACE ";" "|" test_exclusions "${test_exclusions}")
if (test_exclusions)
  set(test_exclusions "(${test_exclusions})")
endif ()
