/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkLayoutGenerator.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkLayoutGenerator
 * @brief Generate Layout xml files out of proxy definitions
 *
 *
 */

#ifndef vtkLayoutGenerator_h
#define vtkLayoutGenerator_h
#include "vtkUIGenerationCoreModule.h" // for exports

#include "vtkObject.h"
#include "vtkSmartPointer.h"

class vtkPVXMLElement;
class vtkSMProxy;

class VTKUIGENERATIONCORE_EXPORT vtkLayoutGenerator
{
public:
  // Generate a layout xml based on the property definitions of proxy
  static vtkSmartPointer<vtkPVXMLElement> GetLayout(vtkSMProxy* proxy);

  //
  static std::string XMLToString(vtkSmartPointer<vtkPVXMLElement> xml);
};
#endif
