/*=========================================================================

   Program: ParaView
   Module:  vtkGenericPropertyWidgetDecorator.h

   Copyright (c) 2005,2006 Sandia Corporation, Kitware Inc.
   All rights reserved.

   ParaView is a free software; you can redistribute it and/or modify it
   under the terms of the ParaView license version 1.2.

   See License_v1.2.txt for the full ParaView license.
   A copy of this license can be obtained by contacting
   Kitware Inc.
   28 Corporate Drive
   Clifton Park, NY 12065
   USA

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHORS OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

========================================================================*/
#ifndef vtkGenericPropertyWidgetDecorator_h
#define vtkGenericPropertyWidgetDecorator_h

#include "vtkGUISupportDecoratorsModule.h"

#include "vtkPropertyWidgetDecorator.h"

#include <memory>

/**
 * vtkGenericPropertyWidgetDecorator is a vtkPropertyWidgetDecorator that
 * supports multiple common use cases from a vtkPropertyWidgetDecorator.
 * The use cases supported are as follows:
 * \li 1. enabling the vtkPropertyWidget when the value of another
 *   property element matches a specific value (disabling otherwise).
 * \li 2. similar to 1, except instead of enabling/disabling the widget is made
 *   "default" when the values match and "advanced" otherwise.
 * \li 3. enabling the vtkPropertyWidget when the array named in the property
 *   has a specified number of components.
 * \li 4. as well as "inverse" of all the above i.e. when the value doesn't
 *   match the specified value.
 * Example usages:
 * \li VectorScaleMode, Stride, Seed, MaximumNumberOfSamplePoints properties on the Glyph proxy.
 */
class VTKGUISUPPORTDECORATORS_EXPORT vtkGenericPropertyWidgetDecorator
  : public vtkPropertyWidgetDecorator
{
public:
  static vtkGenericPropertyWidgetDecorator* New();
  vtkTypeMacro(vtkGenericPropertyWidgetDecorator, vtkPropertyWidgetDecorator);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  void initialize(vtkPVXMLElement* config, vtkSMProxy* proxy) override;

  /**
   * Methods overridden from vtkPropertyWidget.
   */
  bool canShowWidget(bool show_advanced) const override;
  bool enableWidget() const override;

  void updateState();

protected:
  vtkGenericPropertyWidgetDecorator();
  ~vtkGenericPropertyWidgetDecorator() override;

private:
  vtkGenericPropertyWidgetDecorator(const vtkGenericPropertyWidgetDecorator&) = delete;
  void operator=(const vtkGenericPropertyWidgetDecorator&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
