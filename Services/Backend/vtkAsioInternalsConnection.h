/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsConnection.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @class   vtkAsioInternalsConnection
 * @brief   wraps asio::async_read and asio::async_write on a socket.
 *
 * This class follows similar design as the s11n_example::connection class
 * found in asio/src/examples/cpp03/serialization/connection.hpp
 */

#ifndef vtkAsioInternalsConnection_h
#define vtkAsioInternalsConnection_h

#include <deque>
#include <functional>
#include <memory>
#include <string>

// asio
#include "asio.hpp"
namespace io = asio;
using tcp = io::ip::tcp;
using error_code = io::error_code;

class vtkAsioInternalsConnection : public std::enable_shared_from_this<vtkAsioInternalsConnection>
{
public:
  vtkAsioInternalsConnection(io::io_context& ioCtx);
  ~vtkAsioInternalsConnection();
  tcp::socket& GetSocket();

  // Read message from the underlying socket for this connection.
  void AsyncRead();
  // Post an async write
  void PostWrite(std::string&& message);
  // Requests to close the socket. Returns immediately.
  void PostClose();

  // read handler
  std::function<void(const error_code& ec, const std::string& message)> OnRead = nullptr;
  // write handler
  std::function<void(const error_code& ec)> OnWrite = nullptr;
  // error handler
  std::function<void(const error_code& ec)> OnError = nullptr;

protected:
  // do the actual async write
  void AsyncWrite();
  // handle completed read of header.
  void HandleReadHeader(const error_code& ec);
  // handle completed read of data.
  void HandleReadData(const error_code& ec);
  // shutdown and close the socket
  void Close();

private:
  // socket used to read, write data
  tcp::socket Socket;
  // no. of hexadecimal digits that sufficiently represent size of InboundData member
  static constexpr int Headerlength = sizeof(std::string::size_type) * 2;
  // holds inbound header
  char InboundHeader[Headerlength] = {};
  // holds inbound data
  std::string InboundData;
  // used to post execution of read, write, error handlers.
  io::io_context& IOCtx;
  // queue of outbound messages.
  std::deque<std::string> OutboundQueue;
};

using vtkAsioInternalsConnectionPtr = std::shared_ptr<vtkAsioInternalsConnection>;

#endif // vtkAsioInternalsConnection_h

// VTK-HeaderTest-Exclude: vtkAsioInternalsConnection.h
