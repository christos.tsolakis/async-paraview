/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioServicesEngine.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkAsioServicesEngine
 * @brief A concrete implementation of vtkServicesEngine that communicates over asio tcp sockets.
 *
 */

#ifndef vtkAsioServicesEngine_h
#define vtkAsioServicesEngine_h

#include "vtkServicesEngine.h"

#include "vtkAsioInternalsEngineSideTypes.h" // for arg
#include "vtkServicesBackendModule.h"        // for export macro

#include <memory> // for ivar
#include <string> // for arg

struct vtkAsioInternalsRxCommunicator;
class vtkAsioInternalsConnection;
class vtkAsioInternalsEngine;

class VTKSERVICESBACKEND_EXPORT vtkAsioServicesEngine : public vtkServicesEngine
{
public:
  vtkTypeMacro(vtkAsioServicesEngine, vtkServicesEngine);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  static vtkAsioServicesEngine* New();

  ///@{
  /**
   * Get a communicator, connection.
   * @param side corresponds to either `VTKAsioEngineSideType::ServiceSide` or
   * `VTKAsioEngineSideType::EndpointSide`.
   */
  vtkAsioInternalsRxCommunicator* GetCommunicator(VTKAsioEngineSideType side);
  vtkAsioInternalsConnection* GetConnection(VTKAsioEngineSideType side);
  ///@}

  /**
   * Binds a tcp socket to host:port and listen for incoming connections.
   * @param host hostname can be a alphanumeric string or an ipv4 or ipv6 address.
   * @param port a port number or system service.
   */
  std::string Accept(const std::string& host, const std::string& port);

  /**
   * Makes a tcp connection to host:port
   * @param host hostname can be a alphanumeric string or an ipv4 or ipv6 address.
   * @param port a port number or system service.
   */
  rxcpp::observable<bool> Connect(const std::string& host, const std::string& port);

  ///@{
  /**
   * Convenient functions to extract host and port from a url.
   * @param url string in the format protocol://host:port
   */
  static std::string ExtractHost(const std::string& url);
  static std::string ExtractPort(const std::string& url);
  ///@}

  // implement superclass API
  std::string GetProtocol() override;
  std::string GetBuiltinProtocol() override;

protected:
  vtkAsioServicesEngine();
  ~vtkAsioServicesEngine() override;

  // implement superclass API
  std::string InitializeInternal(const std::string& url) override;
  void FinalizeInternal() override;

private:
  vtkAsioServicesEngine(const vtkAsioServicesEngine&) = delete;
  void operator=(const vtkAsioServicesEngine&) = delete;

  std::unique_ptr<vtkAsioInternalsEngine> Internals;
};

#endif // vtkAsioServicesEngine_h
