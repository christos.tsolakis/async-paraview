/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsScheduler.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioInternalsScheduler.h"
#include "vtkServicesAsioLogVerbosity.h"

#include <sstream>
#include <thread>

//----------------------------------------------------------------------------
vtkAsioInternalsScheduler::vtkAsioInternalsScheduler(const std::string& threadName)
{
  auto service_logger_init = [threadName]() {
    auto tid = std::this_thread::get_id();
    vtkLogger::SetThreadName(threadName);
    // introduce ourselves in the logs.
    std::ostringstream introduceMsg;
    introduceMsg << "New thread created (" << tid << ") -> (" << threadName << ")";
    vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << introduceMsg.str());
  };

  rxcpp::schedulers::thread_factory serviceTF = [service_logger_init](
                                                  std::function<void()> start) -> std::thread {
    return std::thread([service_logger_init, start]() {
      service_logger_init();
      start();
    });
  };

  this->Scheduler = std::make_unique<rxcpp::schedulers::new_thread>(serviceTF);
  rxcpp::composite_subscription cs;
  // runs on application main thread.
  cs.add([]() { vtkVLog(VTKSERVICESASIO_LOG_VERBOSITY(), << "Shutdown service thread .. "); });
  // one worker shall be used throughout the lifetime of a service.
  this->Worker = this->Scheduler->create_worker(cs);
}

//----------------------------------------------------------------------------
vtkAsioInternalsScheduler::~vtkAsioInternalsScheduler()
{
  // joins service thread.
  this->Worker.unsubscribe();
}

//----------------------------------------------------------------------------
rxcpp::schedulers::scheduler vtkAsioInternalsScheduler::GetScheduler()
{
  return rxcpp::schedulers::make_same_worker(this->Worker);
}
