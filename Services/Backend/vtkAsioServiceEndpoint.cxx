/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioServiceEndpoint.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAsioServiceEndpoint.h"

#include "vtkAsioInternalsConnection.h"
#include "vtkAsioInternalsRxCommunicator.h"
#include "vtkAsioService.h"
#include "vtkAsioServicesEngine.h"
#include "vtkObjectFactory.h"

#include <chrono>
#include <sstream>
#include <thread>

namespace
{
/**
 * This network packet requests the existence of a service on the server's engine.
 * @param serviceName: name of the service
 * @param serverConnection: a connection to the server.
 */
vtkAsioInternalsNetworkPacket ComposeServiceExistsMessage(const std::string& serviceName)
{
  vtkAsioInternalsNetworkPacket netPacket;
  netPacket.Secret = "null";
  netPacket.Destination = serviceName;
  netPacket.ResponseRequired = true;
  netPacket.Payload =
    std::make_shared<vtkPacket>(vtkNJson{ { vtkAsioService::ServiceExistsKey, serviceName } });
  return netPacket;
}

/**
 * This network packet contains a message that shall be delivered to a service on the server's
 * engine.
 * @param serviceName: name of the service
 * @param message: a vtkPacket that contains a message
 */
vtkAsioInternalsNetworkPacket ComposeServiceMessage(
  const std::string& serviceName, const vtkPacket& message)
{
  vtkAsioInternalsNetworkPacket netPacket;
  netPacket.Secret = "null";
  netPacket.Destination = serviceName;
  netPacket.ResponseRequired = false;
  netPacket.Payload = std::make_shared<vtkPacket>(message);
  return netPacket;
}

/**
 * This network packet contains a request that shall be delivered to a service on the server's
 * engine.
 * @param serviceName: name of the service
 * @param request: a vtkPacket that contains a request
 */
vtkAsioInternalsNetworkPacket ComposeServiceRequest(
  const std::string& serviceName, const vtkPacket& request, const std::string& code)
{
  vtkAsioInternalsNetworkPacket netPacket;
  netPacket.Secret = code;
  netPacket.Destination = serviceName;
  netPacket.ResponseRequired = true;
  netPacket.Payload = std::make_shared<vtkPacket>(request);
  return netPacket;
}

/**
 * Get asio implementation engine.
 * @param endpoint: a vtkAsioServiceEndpoint instance whose engine will be converted to asio impl
 * engine
 */
vtkAsioServicesEngine* GetAsioEngine(const vtkAsioServiceEndpoint* endpoint)
{
  auto engine = vtkAsioServicesEngine::SafeDownCast(endpoint->GetEngine());
  if (engine == nullptr)
  {
    vtkLogF(ERROR, "Engine not of correct type! Expected instance of vtkAsioServicesEngine. Got %s",
      vtkLogIdentifier(endpoint->GetEngine()));
  }
  return engine;
}

/**
 * Output a unique string composed of current thread ID and a timestamp.
 *
 * @note Within the scope of ServicesCore module, all endpoint->Send[Message,Request]
 * are invoked on main thread. The thread ID is not really necessary, however, it's a cautionary
 * measure.
 */
std::string GenerateOneTimeRequestCode()
{
  std::ostringstream tidTime;
  tidTime << std::this_thread::get_id() << ':'
          << std::chrono::steady_clock::now().time_since_epoch().count();
  return tidTime.str();
}
} // end anon namespace

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkAsioServiceEndpoint);

//----------------------------------------------------------------------------
vtkAsioServiceEndpoint::vtkAsioServiceEndpoint() = default;

//----------------------------------------------------------------------------
vtkAsioServiceEndpoint::~vtkAsioServiceEndpoint() = default;

//----------------------------------------------------------------------------
void vtkAsioServiceEndpoint::PrintSelf(ostream& os, vtkIndent indent)
{
  return this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
void vtkAsioServiceEndpoint::InitializeInternal()
{
  if (auto engine = ::GetAsioEngine(this))
  {
    auto receiver = engine->GetCommunicator(this->TAG)->Receiver;
    // Fortuantely, the superclass `vtkServiceEndpoint::DispatchOnChannel`
    // has logic to dispatch packets to the correct subscribers. All it needs are a
    // vtkPacket that wraps content and a channel name where that content must go.
    // Here, this single rxcpp subscription sets up a pipeline which receives
    // network packets that can go to *any* channel (does not concern us).
    // The network packet provides the channel name. Let superclass dispatch content.
    this->ChannelDispatchPipeline =
      receiver
        .get_observable()
        // non-empty channel name with appropriate secret
        .filter([](const vtkAsioInternalsNetworkPacket& netPacket) {
          return !netPacket.Channel.empty() &&
            (netPacket.Secret == "uncached" || netPacket.Secret == "cached");
        })
        // deliver content
        .subscribe([this](const vtkAsioInternalsNetworkPacket& netPacket) {
          const auto& content = (*netPacket.Payload);
          this->DispatchOnChannel(netPacket.Channel, content, netPacket.Secret == "cached");
        });
  }
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkAsioServiceEndpoint::ConnectInternal(
  const std::string& serviceName, const std::string& url)
{
  // cold: make engine connection only after someone subscribes.
  auto source = rxcpp::observable<>::create<bool>([=](const rxcpp::subscriber<bool>& subscriber)
                                                    -> void {
    auto engine = ::GetAsioEngine(this);
    if (engine == nullptr)
    {
      subscriber.on_next(false);
    }

    std::string code = vtkAsioService::ComposeServiceExistsResponseCode(serviceName);
    // these subscribers execute in the io_context thread. const ref should be fine.
    auto& receiver = engine->GetCommunicator(this->TAG)->Receiver;
    auto subscription = receiver.get_observable()
                          .filter([code](const vtkAsioInternalsNetworkPacket& netPacket) {
                            return netPacket.Secret == code && netPacket.Destination == "null";
                          })
                          .subscribe([subscriber](const auto& netPacket) {
                            const vtkNJson& json = netPacket.Payload->GetJSON();
                            const bool exists = json[vtkAsioService::ServiceExistsKey].get<bool>();
                            subscriber.on_next(exists);
                          });
    subscriber.add([=]() { subscription.unsubscribe(); });

    // initiate a connection if not already done.
    if (engine->GetConnection(vtkAsioServiceEndpoint::TAG) == nullptr)
    {
      std::string host = vtkAsioServicesEngine::ExtractHost(url);
      std::string port = vtkAsioServicesEngine::ExtractPort(url);
      engine->Connect(host, port).subscribe([serviceName, subscriber, engine](const bool& status) {
        if (status)
        {
          auto serverConnection = engine->GetConnection(vtkAsioServiceEndpoint::TAG);
          serverConnection->PostWrite(ToBytes(::ComposeServiceExistsMessage(serviceName)));
        }
        else
        {
          subscriber.on_next(false);
        }
      });
    }
    else
    {
      auto serverConnection = engine->GetConnection(vtkAsioServiceEndpoint::TAG);
      serverConnection->PostWrite(ToBytes(::ComposeServiceExistsMessage(serviceName)));
    }
  });

  return source.take(1);
}

//----------------------------------------------------------------------------
void vtkAsioServiceEndpoint::ShutdownInternal()
{
  // remove channel dispatch pipeline that InitializeInternal created.
  this->ChannelDispatchPipeline.unsubscribe();
}

//----------------------------------------------------------------------------
void vtkAsioServiceEndpoint::SendMessageInternal(const vtkPacket& packet) const
{
  if (auto engine = ::GetAsioEngine(this))
  {
    auto serverConnection = engine->GetConnection(vtkAsioServiceEndpoint::TAG);
    serverConnection->PostWrite(ToBytes(::ComposeServiceMessage(this->GetServiceName(), packet)));
  }
}

//----------------------------------------------------------------------------
rxcpp::observable<vtkPacket> vtkAsioServiceEndpoint::SendRequestInternal(
  const vtkPacket& packet) const
{
  auto source =
    rxcpp::observable<>::create<vtkPacket>([=](const rxcpp::subscriber<vtkPacket>& subscriber) {
      if (auto engine = ::GetAsioEngine(this))
      {
        // setup response handler
        std::string code = ::GenerateOneTimeRequestCode();
        auto& receiver = engine->GetCommunicator(this->TAG)->Receiver;
        auto subscription =
          receiver
            .get_observable()
            // verify secret code
            .filter([code](const vtkAsioInternalsNetworkPacket& np) { return np.Secret == code; })
            // emit response to caller
            .subscribe([subscriber](const auto& netPacket) {
              const vtkPacket& response = (*netPacket.Payload);
              // If the server could not perform the requested operation it will fill the
              // __vtk_error_message__ field.
              if (response.GetJSON().contains("__vtk_error_message__"))
              {
                const std::string errorMsg =
                  response.GetJSON().at("__vtk_error_message__").get<std::string>();
                subscriber.on_error(rxcpp::rxu::make_error_ptr(std::runtime_error(errorMsg)));
              }
              else
              {
                subscriber.on_next(response);
              }
            });
        subscriber.add([=]() { subscription.unsubscribe(); });

        // send the request through our connection to the service
        auto serverConnection = engine->GetConnection(vtkAsioServiceEndpoint::TAG);
        serverConnection->PostWrite(
          ToBytes(::ComposeServiceRequest(this->GetServiceName(), packet, code)));
      }
      else
      {
        subscriber.on_next(vtkPacket());
      }
    });
  return source.take(1);
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkAsioServiceEndpoint::SubscribeInternal(const std::string& channel)
{
  auto source = rxcpp::observable<>::create<bool>([=](const rxcpp::subscriber<bool>& subscriber) {
    if (auto engine = ::GetAsioEngine(this))
    {
      const std::string responseKey =
        vtkAsioService::ComposeChannelSubscribeResponseCode(this->GetServiceName(), channel);
      vtkPacket subscribeReqst(vtkNJson{ { vtkAsioService::ChannelSubscribeKey, channel } });
      auto subscription =
        this->SendRequestInternal(subscribeReqst).subscribe([=](const vtkPacket& reply) {
          const auto& json = reply.GetJSON();
          const bool success = json[responseKey].get<bool>();
          subscriber.on_next(success);
        });
      subscriber.add([=]() { subscription.unsubscribe(); });
    }
    else
    {
      subscriber.on_next(false);
    }
  });
  return source.take(1);
}

//----------------------------------------------------------------------------
void vtkAsioServiceEndpoint::UnsubscribeInternal(const std::string& channel)
{
  auto source = rxcpp::observable<>::create<bool>([=](const rxcpp::subscriber<bool>& subscriber) {
    if (auto engine = ::GetAsioEngine(this))
    {
      const std::string responseKey =
        vtkAsioService::ComposeChannelUnsubscribeResponseCode(this->GetServiceName(), channel);
      vtkPacket unsubRequest(vtkNJson{ { vtkAsioService::ChannelUnsubscribeKey, channel } });
      auto subscription =
        this->SendRequestInternal(unsubRequest).subscribe([=](const vtkPacket& reply) {
          const auto& json = reply.GetJSON();
          const bool success = json[responseKey].get<bool>();
          subscriber.on_next(success);
        });
      subscriber.add([=]() { subscription.unsubscribe(); });
    }
    else
    {
      subscriber.on_next(false);
    }
  });
  // There is really no point in returning this observable. It is not needed by anyone.
  source.take(1).as_blocking().subscribe(
    [](bool value) { vtkLogIfF(ERROR, !value, "Failed to unsubscribe"); });
}
