/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioService.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkAsioService
 * @brief A concrete implementation of vtkService that communicates with asio tcp sockets.
 *
 */

#ifndef vtkAsioService_h
#define vtkAsioService_h

#include "vtkService.h"

#include "vtkAsioInternalsEngineSideTypes.h" // ivar
#include "vtkServicesBackendModule.h"        // for export macro

#include <memory> // ivar

class VTKSERVICESBACKEND_EXPORT vtkAsioService : public vtkService
{
public:
  vtkTypeMacro(vtkAsioService, vtkService);
  void PrintSelf(ostream& os, vtkIndent indent) override;
  static vtkAsioService* New();

  static std::string ComposeServiceExistsResponseCode(const std::string& serviceName)
  {
    return ServiceExistsKey + ':' + serviceName;
  }

  static std::string ComposeChannelSubscribeResponseCode(
    const std::string& serviceName, const std::string& channelName)
  {
    return ChannelSubscribeKey + ':' + serviceName + ':' + channelName;
  }

  static std::string ComposeChannelUnsubscribeResponseCode(
    const std::string& serviceName, const std::string& channelName)
  {
    return ChannelUnsubscribeKey + ':' + serviceName + ':' + channelName;
  }

  static const std::string ServiceExistsKey;
  static const std::string ChannelSubscribeKey;
  static const std::string ChannelUnsubscribeKey;

  // Makes an rxcpp subscription to the channel observable i.e,
  // vtkService::GetChannelObservable(channelName)
  rxcpp::composite_subscription MakeChannelSubscription(const std::string& channelName);

  // Implement superclass API
  rxcpp::observe_on_one_worker GetRunLoopScheduler() const override;

protected:
  vtkAsioService();
  ~vtkAsioService() override;

  // Implement superclass API
  void InitializeInternal() override;
  bool StartInternal() override;
  void ShutdownInternal() override;

  // used to observe incoming messages from a connection.
  rxcpp::composite_subscription Subscription;

private:
  vtkAsioService(const vtkAsioService&) = delete;
  void operator=(const vtkAsioService&) = delete;

  class vtkInternals;
  std::shared_ptr<vtkInternals> Internals;
  static constexpr VTKAsioEngineSideType TAG = VTKAsioEngineSideType::VTKAEST_ServiceSide;
};

#endif // vtkAsioService_h
