/*=========================================================================

  Program:   ParaView
  Module:    vtkAsioInternalsScheduler.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

/**
 * @class vtkAsioInternalsScheduler
 * @brief Sets up an rxcpp worker based on custom rxcpp thread factory.
 *        A single thread is reused for subsequent workers with make_same_worker
 *        These ideas were presented in https://github.com/ReactiveX/RxCpp/issues/468
 */

#ifndef vtkAsioInternalsScheduler_h
#define vtkAsioInternalsScheduler_h

#include <memory> // for std::unique_ptr
#include <string> // for arg

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkAsioInternalsScheduler
{
public:
  explicit vtkAsioInternalsScheduler(const std::string& threadName);
  ~vtkAsioInternalsScheduler();
  rxcpp::schedulers::scheduler GetScheduler();

private:
  std::unique_ptr<rxcpp::schedulers::new_thread> Scheduler;
  rxcpp::schedulers::worker Worker;
};

#endif // vtkAsioInternalsScheduler_h

// VTK-HeaderTest-Exclude: vtkAsioInternalsScheduler.h
