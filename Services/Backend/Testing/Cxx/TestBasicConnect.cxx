/*=========================================================================

  Program:   ParaView
  Module:    TestBasicConnect.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include <chrono>

#include <vtkNew.h>
#include <vtkService.h>
#include <vtkServiceEndpoint.h>
#include <vtkServicesEngine.h>

#define CHECK_ERROR(header, status)                                                                \
  if (!status)                                                                                     \
  {                                                                                                \
    vtkLog(ERROR, << header << ". Error occurred.");                                               \
    engine->Finalize();                                                                            \
    return 1;                                                                                      \
  }

int TestBasicConnect(int /*argc*/, char* /*argv*/[])
{

  bool success = false;
  vtkNew<vtkServicesEngine> engine;
  engine->Initialize(engine->GetBuiltinProtocol());
  auto ds = engine->CreateService("services.data");
  success = ds->Start();
  auto rs = engine->CreateService("services.render");
  success &= rs->Start();
  auto dsep = engine->CreateServiceEndpoint("services.data");
  auto rsep = engine->CreateServiceEndpoint("services.render");
  CHECK_ERROR("Service startup", success)

  auto o1 = dsep->Connect();
  auto o2 = rsep->Connect();
  auto connected = o1.combine_latest(o2)
                     .observe_on(engine->GetCoordination())
                     .map([](const std::tuple<bool, bool>& status) {
                       return std::get<0>(status) && std::get<1>(status);
                     });
  success &= engine->Await(connected.as_dynamic());
  CHECK_ERROR("Connect endpoints with services", success)

  engine->Finalize();
  return 0;
}
