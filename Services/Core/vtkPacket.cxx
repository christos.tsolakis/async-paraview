/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkPacket.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkPacket.h"

#include "vtkObjectFactory.h"

#include <sstream>

#include <vtk_fmt.h>
// clang-format off
#include VTK_FMT(fmt/core.h)
// clang-format on

//----------------------------------------------------------------------------
vtkPacket::vtkPacket()
  : JSON(std::make_shared<vtkNJson>())
{
}

//----------------------------------------------------------------------------
vtkPacket::vtkPacket(const vtkNJson& json)
  : JSON(std::make_shared<vtkNJson>(json))
{
}

//----------------------------------------------------------------------------
vtkPacket::vtkPacket(vtkNJson&& json)
  : JSON(std::make_shared<vtkNJson>(std::move(json)))
{
}

//----------------------------------------------------------------------------
vtkPacket::vtkPacket(
  const vtkNJson& json, const std::map<std::string, vtkSmartPointer<vtkObject>>& payload)
  : vtkPacket(json)
{
  this->Payload = payload;
}

//----------------------------------------------------------------------------
vtkPacket::vtkPacket(
  vtkNJson&& json, const std::map<std::string, vtkSmartPointer<vtkObject>>& payload)
  : vtkPacket(std::move(json))
{
  this->Payload = payload;
}

//----------------------------------------------------------------------------
const vtkNJson& vtkPacket::GetJSON() const
{
  return *this->JSON;
}

//----------------------------------------------------------------------------
void vtkPacket::Print() const
{
  this->Print(cout, 2);
}

//----------------------------------------------------------------------------
void vtkPacket::Print(std::ostream& ostream, int spaces) const
{
  ostream << this->JSON->dump(spaces) << endl;
}

//----------------------------------------------------------------------------
std::string vtkPacket::ToString(int spaces) const
{
  return this->JSON->dump(spaces);
}

//----------------------------------------------------------------------------
std::vector<uint8_t> vtkPacket::GetBinary() const
{
  return vtkNJson::to_cbor(*this->JSON);
}

//----------------------------------------------------------------------------
void vtkPacket::SetBinary(const std::vector<uint8_t>& buffer)
{
  *this->JSON = vtkNJson::from_cbor(buffer);
}

//----------------------------------------------------------------------------
void to_json(nlohmann::json& json, const vtkPacket& packet)
{
  json = packet.GetJSON();
  assert(packet.GetPayload().empty());
}

//----------------------------------------------------------------------------
void from_json(const nlohmann::json& json, vtkPacket& packet)
{
  packet = vtkPacket{ json };
}
