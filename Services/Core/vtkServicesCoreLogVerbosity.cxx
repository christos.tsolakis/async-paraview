/*=========================================================================

  Program:   ParaView
  Module:    vtkServicesCoreLogVerbosity.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkServicesCoreLogVerbosity.h"
#include "vtksys/SystemTools.hxx"

namespace
{
// executed when this library is loaded.
vtkLogger::Verbosity GetInitialServicesCoreVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::ServicesCore module.
  const char* VerbosityKey = "VTKSERVICESCORE_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    const auto verbosity = vtkLogger::ConvertToVerbosity(verbosity_str);
    if (verbosity > vtkLogger::VERBOSITY_INVALID)
    {
      return verbosity;
    }
  }
  return vtkLogger::VERBOSITY_TRACE;
}

// executed when this library is loaded.
vtkLogger::Verbosity GetInitialServicesCoreProviderVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::ServicesCore module.
  const char* VerbosityKey = "VTKSERVICESCORE_PROVIDER_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    const auto verbosity = vtkLogger::ConvertToVerbosity(verbosity_str);
    if (verbosity > vtkLogger::VERBOSITY_INVALID)
    {
      return verbosity;
    }
  }
  return vtkLogger::VERBOSITY_TRACE;
}

thread_local vtkLogger::Verbosity ServicesCoreVerbosity = GetInitialServicesCoreVerbosity();
thread_local vtkLogger::Verbosity ServicesCoreProviderVerbosity =
  GetInitialServicesCoreProviderVerbosity();

}

//----------------------------------------------------------------------------
vtkLogger::Verbosity vtkServicesCoreLogVerbosity::GetVerbosity()
{
  return ::ServicesCoreVerbosity;
}

//----------------------------------------------------------------------------
vtkLogger::Verbosity vtkServicesCoreLogVerbosity::GetProviderVerbosity()
{
  return ::ServicesCoreProviderVerbosity;
}

//----------------------------------------------------------------------------
void vtkServicesCoreLogVerbosity::SetVerbosity(vtkLogger::Verbosity verbosity)
{
  if (verbosity > vtkLogger::VERBOSITY_INVALID)
  {
    ::ServicesCoreVerbosity = verbosity;
  }
}

//----------------------------------------------------------------------------
void vtkServicesCoreLogVerbosity::SetProviderVerbosity(vtkLogger::Verbosity verbosity)
{
  if (verbosity > vtkLogger::VERBOSITY_INVALID)
  {
    ::ServicesCoreProviderVerbosity = verbosity;
  }
}
