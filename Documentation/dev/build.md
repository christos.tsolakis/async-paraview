# Build Instructions for ParaView Async

Currently, there are no pre-built binaries for the project.
As the project evolve we will make some effort to streamline build process and binary distribution.

## Prerequisites
The ParaView asynchronous backend supports two different communication backends.
  - [thallium](https://mochi.readthedocs.io/en/latest/thallium.html) which supports Linux and MacOS.
  - [asio](https://think-async.com/Asio/) which is supported on all platforms.

To build this project using the thallium backend follow thallium's build [instructions](https://mochi.readthedocs.io/en/latest/installing.html)>
The asio library is header-only and included in this project no further steps are required.

## Step by step build process
These instructions work on Linux, Windows and MacOS. On Windows, please execute these commands in Developer Powershell for VS2022

__Getting the repository__

```bash
git clone --recursive https://gitlab.kitware.com/async/async-paraview.git
```

__Build ParaView Asynchronous__

The basic configuration can be achieved via:

```bash
cmake -G Ninja -S ./async-paraview -B ./build -DAPV_USE_PYTHON=ON -DAPV_USE_VTKM=OFF
```

If using `cmake` version > 3.21 cmake [presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html) are also available.
Currently there are three presets:

- `async-base` : Same as above but enables testing
- `async-asio-only` : `async-base` + use only the ASIO backend for communication between the client and the server.
- `async-app-backend` : configuration of the project for the `async_paraview_app`

To configure using a preset:

```bash
cmake --preset async-asio-only -S ./async-paraview -B ./build
```

Finally, to build the project:

```
cmake --build ./build
```

Note:
- __macOS__: When system python is used, make sure to define these additional run-path variables in the configuration step:
    ```bash
    -DCMAKE_BUILD_RPATH="/Applications/Xcode.app/Contents/Developer/Library/Frameworks" \
    -DCMAKE_INSTALL_RPATH="/Applications/Xcode.app/Contents/Developer/Library/Frameworks"
    ```
- __Python version:__ If a specific version of Python is preferred you should `export PYTHONPATH=./build/lib/pythonx.y/site-packages` to point to that python.
- __Communication Backend:__ To enable a specific backend use `-DAPV_ENABLE_THALLIUM=ON`/OFF or `-APV_ENABLE_ASIO=ON`/OFF during configuration time. If both backends are enabled you can select which to use upon runtime using the enviromental `APV_COMMUNICATION_BACKEND` and setting it to `ASIO` or `THALLIUM` before launching the application.
- __VTKm__: It is currently disabled until the VTKm filters are abort-aware.
- __Ospray:__ For raytracing [get](https://github.com/ospray/ospray/releases/tag/v2.9.0) ospray binaries and set `-DAPV_ENABLE_RAYTRACING=ON` and  `-Dospray_DIR` to the appropriate location. For Linux  it is  `-Dospray_DIR="<extracted location>/ospray-2.9.0.x86_64.linux/lib/cmake/ospray-2.9.0"`. When running any application utilizing raytracing you may need to update the library path. For Linux: `export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:<extracted location>/ospray-2.9.0.x86_64.linux/lib/`
- __MPI:__ Set `-DAPV_USE_MPI=ON` in the cmake configure step.
- __Software video encoders (CPU)__: `nasm` is needed to build the `libvpx` codec sdk for VP9 encoder and decoder.
You may download `nasm` from https://www.nasm.us/ or your distribution package manager.
- __Hardware accelerated video encoders (GPU)__: There are no compile time dependencies. You can build the project on a machine without an NVIDIA card. The only runtime requirement is an NVIDIA GPU with NVENC capabilities and a working nvidia driver.
Please visit https://developer.nvidia.com/video-encode-and-decode-gpu-support-matrix-new to check if your GPU is supported.

Less common, but potentially useful variables are:

- `APV_LOGGING_TIME_PRECISION` (default `3`): Change the precision of
  times output. Possible values are 3 for ms, 6 for us, 9 for ns.


## Running trame examples
In order to run a trame application with the ParaView
asynchronous backend, you will need to setup a virtual-environment with the
trame library to use.
For that double check which version of Python your ParaView was built against
by running `<build-directory>/bin/apvpython`.

Then you should be able to run the following to create your
virtual-environment.

```bash
python3.9 -m venv work/pvasync-env
source ./work/pvasync-env/bin/activate # Read note for windows specific command
python3 -m pip install -U pip
python3 -m pip install trame

export PV_VENV=$PWD/work/pvasync-env # Read note for windows specific command
```
At that point you are ready to run any trame application with the asynchronous backend.

If you want to quickly test the reference example you can run the following

```bash
curl -L -O https://gitlab.kitware.com/async/paraview-async-examples/-/raw/main/async/python/wavelet.py
./build/bin/apvpython ./wavelet.py
```

Note:
- __Windows__: If you executed `python` or `python3` in powershell and got redirected to MS store, try `py` instead. Even inside the virtual environment use `py`.
- __Windows__: Run `./work/pvasync-env/Scripts/activate.ps1` to initialize the python environment in powershell with [script execution priviliges](https://learn.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_execution_policies?view=powershell-7.3#change-the-execution-policy). Basically elevate your privilege with `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser`
- __Windows__: Set `PV_VENV` like so `$env:PV_VENV="$pwd\work\pvasync-env"`
- __Windows__: Use `curl.exe` to download the wavelet.py instead of the alias `curl`.
