/*=========================================================================

  Program:   ParaView
  Module:    vtkCocoaRenderWindowThreadSafe.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkCocoaRenderWindowThreadSafe
 * @brief   Thread safe factory override for VTK OpenGL rendering on Mac OSX.
 * @warning There is negligible overhead for switching threads every render. Unfortunately, Apple
 * will not be removing the API main thread restriction and OpenGL is long deprecated in Mac OSX.
 */

#ifndef vtkCocoaRenderWindowThreadSafe_h
#define vtkCocoaRenderWindowThreadSafe_h

#include "vtkCocoaRenderWindow.h"

#include "vtkRemotingServerManagerViewsModule.h" // for export macro

#include <thread> // for ivar

class VTKREMOTINGSERVERMANAGERVIEWS_EXPORT vtkCocoaRenderWindowThreadSafe
  : public vtkCocoaRenderWindow
{
public:
  static vtkCocoaRenderWindowThreadSafe* New();
  vtkTypeMacro(vtkCocoaRenderWindowThreadSafe, vtkCocoaRenderWindow);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * This method is called during each `Render` to make sure NSOpenGLContext has an NSView
   * and an up to date OpenGL context. The related cocoa API is NSOpenGLContext:setView and
   * NSOpenGLContext:update. Here, we enqueue superclass method on the services engine's scheduler
   * to ensure those two cocoa methods are run from the main thread.
   * Otherwise, `apvserver` will crash with an error `-[NSOpenGLContext setView:] must be called
   * from the main thread.`
   */
  void Start() override;

protected:
  vtkCocoaRenderWindowThreadSafe();
  ~vtkCocoaRenderWindowThreadSafe() override;

  std::thread::id MainTID;

private:
  vtkCocoaRenderWindowThreadSafe(const vtkCocoaRenderWindowThreadSafe&) = delete;
  void operator=(vtkCocoaRenderWindowThreadSafe&) = delete;
};

#endif
