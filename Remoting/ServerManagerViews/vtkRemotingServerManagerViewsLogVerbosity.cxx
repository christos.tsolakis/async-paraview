/*=========================================================================

  Program:   ParaView
  Module:    vtkRemotingServerManagerViewsLogVerbosity.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkRemotingServerManagerViewsLogVerbosity.h"
#include "vtksys/SystemTools.hxx"

namespace
{
// executed when this library is loaded.
vtkLogger::Verbosity GetInitialRemotingServerManagerViewsVerbosity()
{
  // Find an environment variable that specifies logger verbosity for
  // the ParaView::RemotingServerManagerViews module.
  const char* VerbosityKey = "VTKREMOTINGSERVERMANAGERVIEWS_LOG_VERBOSITY";
  if (vtksys::SystemTools::HasEnv(VerbosityKey))
  {
    const char* verbosity_str = vtksys::SystemTools::GetEnv(VerbosityKey);
    const auto verbosity = vtkLogger::ConvertToVerbosity(verbosity_str);
    if (verbosity > vtkLogger::VERBOSITY_INVALID)
    {
      return verbosity;
    }
  }
  return vtkLogger::VERBOSITY_TRACE;
}

thread_local vtkLogger::Verbosity RemotingServerManagerViewsVerbosity =
  GetInitialRemotingServerManagerViewsVerbosity();
}

//----------------------------------------------------------------------------
vtkLogger::Verbosity vtkRemotingServerManagerViewsLogVerbosity::GetVerbosity()
{
  return ::RemotingServerManagerViewsVerbosity;
}

//----------------------------------------------------------------------------
void vtkRemotingServerManagerViewsLogVerbosity::SetVerbosity(vtkLogger::Verbosity verbosity)
{
  if (verbosity > vtkLogger::VERBOSITY_INVALID)
  {
    ::RemotingServerManagerViewsVerbosity = verbosity;
  }
}
