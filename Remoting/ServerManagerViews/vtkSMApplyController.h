/*=========================================================================

  Program:   ParaView
  Module:    vtkSMApplyController.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkSMApplyController
 * @brief
 *
 */

#ifndef vtkSMApplyController_h
#define vtkSMApplyController_h

#include "vtkCommand.h"
#include "vtkObject.h"
#include "vtkRemotingServerManagerViewsModule.h" // for exports

#include <memory> // for std::unique_ptr

class vtkSMProxy;
class vtkSMSessionProxyManager;
class vtkSMSourceProxy;
class vtkSMViewProxy;

class VTKREMOTINGSERVERMANAGERVIEWS_EXPORT vtkSMApplyController : public vtkObject
{
public:
  static vtkSMApplyController* New();
  vtkTypeMacro(vtkSMApplyController, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Use this method to tag a new source proxy so that on create new
   * representation to show the data on apply in preferred view.
   */
  static void MarkShowOnApply(vtkSMProxy* proxy);
  static void UnmarkShowOnApply(vtkSMProxy* proxy);
  static bool IsMarkedShowOnApply(vtkSMProxy* proxy);
  ///@}

  /**
   * Execute the apply logic for a particular session.
   */
  virtual void Apply(vtkSMSessionProxyManager* pxm);

  /**
   * Inform interested observers that "Apply was executed". In particular, an
   * ApplyEvent is fired for each view when `vtkSMViewProxy::Update()` emits,
   * which is when the data processing part of an update for a given view is
   * completed. This also means that data information if up-to-date.
   */
  enum
  {
    ApplyEvent = vtkCommand::UserEvent + 3003,
  };

  /**
   * Handle change to scalar coloring.
   *
   * Argument is the representation proxy that is being updated.
   */
  virtual void ApplyScalarColoring(vtkSMProxy* repr);

protected:
  vtkSMApplyController();
  ~vtkSMApplyController() override;

  vtkSMViewProxy* GetActiveView(vtkSMSessionProxyManager* pxm) const;

  /**
   * Hides all inputs of the producer in a view.
   */
  virtual void HideInputs(vtkSMProxy* producer, vtkSMViewProxy* view);

  /**
   * Updates the view and resets client-side camera to a view frustum that spans
   * the bounding box of all visible geometry on server-side.
   */
  virtual void ResetCamera(vtkSMViewProxy* view);

  /**
   * Shows a representation by turning on it's visibility property.
   */
  virtual void Show(vtkSMSourceProxy* producer);

  /**
   * Apply(vtkSMSessionProxyManager* pxm) performs two passes for all proxies that have some kind of
   annotation.
   * The operations on each pass depends currently on the type of the proxy.
   * - SourceProxies: Show a representation for the producer. If a representation doesn't exist, it
   will
   * be created and made visible.
   * - Representations: Update scalar transfer functions if this the representation has been
   annotated with UPDATE_COLORING=TRUE
   TODO
   */
  virtual void FirstPass(vtkSMProxy* proxy);

  /**
   * Apply(vtkSMSessionProxyManager* pxm) performs two passes for all proxies that have some kind of
   * annotation.
   * - SourceProxie hides the inputs of all newly-created filters.
   * - Representations this pass is empty
   */
  virtual void SecondPass(vtkSMSessionProxyManager* pxm);

  /**
   * Sets a default color for the representation of the \p producer 's output  on port \p outputPort
   * in \p view. Currently, it will pick the first scalar varable accociated with the points
   * of outputPort's representation.
   */
  virtual void InitializeScalarColoring(
    vtkSMProxy* producer, unsigned int outputPort, vtkSMViewProxy* view);

private:
  vtkSMApplyController(const vtkSMApplyController&) = delete;
  void operator=(const vtkSMApplyController&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
