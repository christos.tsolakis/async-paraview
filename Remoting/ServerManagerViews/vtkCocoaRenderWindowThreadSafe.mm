/*=========================================================================

  Program:   ParaView
  Module:    vtkCocoaRenderWindowThreadSafe.mm

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkCocoaRenderWindowThreadSafe.h"
#include "vtkObjectFactory.h"
#include "vtkPVCoreApplication.h"
#include "vtkRemotingCoreUtilities.h"
#include "vtkServicesEngine.h"

namespace
{
bool StartAndReleaseContext(vtkSmartPointer<vtkCocoaRenderWindowThreadSafe> window)
{
  window->Superclass::Start();
  // releases OpenGL context on this thread.
  window->ReleaseCurrent();
  return true;
}
}

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkCocoaRenderWindowThreadSafe);

//-----------------------------------------------------------------------------
vtkCocoaRenderWindowThreadSafe::vtkCocoaRenderWindowThreadSafe()
  : MainTID(std::this_thread::get_id())
{
}

//-----------------------------------------------------------------------------
vtkCocoaRenderWindowThreadSafe::~vtkCocoaRenderWindowThreadSafe() = default;

//-----------------------------------------------------------------------------
void vtkCocoaRenderWindowThreadSafe::PrintSelf(ostream& os, vtkIndent indent) {}

//-----------------------------------------------------------------------------
void vtkCocoaRenderWindowThreadSafe::Start()
{
  if (std::this_thread::get_id() == this->MainTID)
  {
    return this->Superclass::Start();
  }
  else
  {
    auto pvapp = vtkPVCoreApplication::GetInstance();
    auto engine = pvapp->GetServicesEngine();
    auto coordination = engine->GetCoordination();
    // Start calls NSOpenGLContext:setView and NSOpenGLContext:update.
    auto val =
      vtkRemotingCoreUtilities::RunAsBlocking(&::StartAndReleaseContext, coordination, this);
    (void)val;
    // make context current for OpenGL functions on caller thread.
    this->MakeCurrent();
  }
}
