/*=========================================================================

  Program:   ParaView
  Module:    vtkSMState.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class   vtkSMState
 * @brief   Encapsulates the state of an Async ParaView application.
 *
 * This class represents the entire state in an object. It is meant to help
 * developers use custom state files (.xml, .json, .yaml).
 *
 * You may query the application version that produced the state and proxy states using
 * the public functions.
 *
 * vtkSMState internally maintains a json representation of all proxies,
 * their properties and their values.
 *
 * The DumpJSON is one way to obtain a json representation of the state
 * Similarly, DumpXML and DumpYAML can be implemented by custom applications.
 *
 * @sa vtkSMProxyState, vtkSMPropertyState
 */

#ifndef vtkSMState_h
#define vtkSMState_h

#include "vtkNJson.h"                       // for vtkNJson
#include "vtkRemotingServerManagerModule.h" // for export macro
#include "vtkSMProxyState.h"                // for proxies
#include "vtkType.h"                        // for vtkTypeUInt32

#include <memory>  // for unique_ptr
#include <ostream> // for print
#include <string>  // for names

class VTKREMOTINGSERVERMANAGER_EXPORT vtkSMState
{
public:
  /**
   * Default constructor to create an empty vtkSMState.
   */
  vtkSMState();

  ///@{
  /**
   * Constructors to create a vtkSMState from a vtkNJson object.
   */
  vtkSMState(const vtkNJson& json);
  vtkSMState(vtkNJson&& json);
  ///@}

  ~vtkSMState();

  vtkSMState(const vtkSMState&) = default;
  vtkSMState& operator=(const vtkSMState&) = default;

  vtkSMState(vtkSMState&&) noexcept = default;
  vtkSMState& operator=(vtkSMState&&) noexcept = default;

  const vtkNJson& GetJSON() const;
  void Print(std::ostream& out) const;
  std::string DumpJSON() const;

  int GetVersionMajor() const;
  void SetVersionMajor(int value);

  int GetVersionMinor() const;
  void SetVersionMinor(int value);

  int GetVersionPatch() const;
  void SetVersionPatch(int value);

  vtkSMProxyState GetProxyState(const std::string& proxyName) const;
  vtkSMProxyState GetProxyState(const char* proxyName) const;
  vtkSMProxyState GetProxyState(vtkTypeUInt32 uid) const;
  void SetProxyState(
    const std::string& proxyName, const vtkTypeUInt32& uid, const vtkSMProxyState& state);

private:
  std::shared_ptr<vtkNJson> JSON;
};

inline ostream& operator<<(ostream& os, vtkSMState& state)
{
  state.Print(os);
  return os;
}

#endif // vtkSMState_h
