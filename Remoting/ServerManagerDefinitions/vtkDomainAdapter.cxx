/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkDomainAdapter.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#include "vtkDomainAdapter.h"
#include "vtkObjectFactory.h"
#include "vtkSMDocumentation.h"
#include "vtkSMDomain.h"
#include "vtkSMVectorProperty.h"
#include "vtkSmartPointer.h"

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkDomainAdapter);

//-----------------------------------------------------------------------------
vtkDomainAdapter::vtkDomainAdapter() = default;

//-----------------------------------------------------------------------------
vtkDomainAdapter::~vtkDomainAdapter() = default;

//-----------------------------------------------------------------------------
void vtkDomainAdapter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "SMDomain " << this->SMDomain;
  this->SMDomain->PrintSelf(os, indent.GetNextIndent());
}

//-----------------------------------------------------------------------------
void vtkDomainAdapter::SetSMDomain(vtkSMDomain* domain)
{
  this->SMDomain = domain;
}

//------------------------------------------------------------------------------
std::string vtkDomainAdapter::GetName() const
{
  return this->SMDomain->GetXMLName();
}

//------------------------------------------------------------------------------
std::string vtkDomainAdapter::GetClassName() const
{
  return this->SMDomain->GetClassName();
}
