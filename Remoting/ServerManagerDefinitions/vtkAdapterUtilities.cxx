/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkAdapterUtilities.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAdapterUtilities.h"
#include "vtkCollection.h"
#include "vtkNew.h"
#include "vtkPVXMLElement.h"

std::vector<std::string> vtkAdapterUtilities::GetDecoratorNames(vtkPVXMLElement* hints)
{
  std::vector<std::string> decoratorTypes;
  vtkNew<vtkCollection> collection;
  if (hints)
  {
    hints->FindNestedElementByName("PropertyWidgetDecorator", collection.GetPointer());
  }
  for (int cc = 0; cc < collection->GetNumberOfItems(); cc++)
  {
    vtkPVXMLElement* elem = vtkPVXMLElement::SafeDownCast(collection->GetItemAsObject(cc));
    if (elem && elem->GetAttribute("type"))
    {
      decoratorTypes.push_back(elem->GetAttribute("type"));
    }
  }
  return decoratorTypes;
}
