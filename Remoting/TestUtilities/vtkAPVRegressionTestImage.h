/*=========================================================================

  Program:   ParaView
  Module:    vtkAPVRegressionTestImage.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#ifndef vtkAPVRegressionTestImage_h
#define vtkAPVRegressionTestImage_h

// Includes and a macro necessary for saving the image produced by a cxx
// example program. This capability is critical for regression testing.
// This function returns 1 if test passed, 0 if test failed. It accepts a
// view proxy, vtkPVCoreApplication and a run loop.

#include "vtkRegressionTestImage.h"

#include "vtkPythonObservableWrapper.h"     //for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkRemotingTestUtilitiesModule.h" // for export macro

#include "vtkTesting.h"
#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkPVCoreApplication;
class vtkSMViewProxy;

class VTKREMOTINGTESTUTILITIES_EXPORT vtkAPVRegressionTester : public vtkObject
{
public:
  static vtkAPVRegressionTester* New();
  vtkTypeMacro(vtkAPVRegressionTester, vtkObject);
  void PrintSelf(ostream&, vtkIndent) override;

  // Capture screenshot and return true if successful.
  static rxcpp::observable<bool> RenderAndCaptureView(vtkSMViewProxy* view);
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(bool, static RenderAndCaptureView(vtkSMViewProxy* view))

  // Renders one frame and validates against a baseline image. It also begins interaction
  // if the '-I' argument was present in the command-line arguments.
  static int Test(int argc, char* argv[], vtkSMViewProxy* view, vtkPVCoreApplication* pvapp,
    vtkrxcpp::schedulers::run_loop& rlp, double threshold);

  // Renders one frame and validates against a baseline image. It also begins interaction
  // if the '-I' argument was present in the command-line arguments.
  static int Test(
    int argc, char* argv[], vtkSMViewProxy* view, vtkPVCoreApplication* pvapp, double threshold);

  /** Validates against a baseline image. This signature is useful from Python in combination with
   * RenderAndCaptureView()
   * Ex:
   * await <asFuture>(vtkAPVRegressionTester.RenderAndCaptureView(view))
   * vtkAPVRegressionTester.Test(sys.argv, view, app, threshold)
   */
  static int Test(std::vector<std::string> args, vtkSMViewProxy* view, vtkPVCoreApplication* pvapp,
    double threshold);

protected:
  vtkAPVRegressionTester();
  ~vtkAPVRegressionTester() override;

private:
  vtkAPVRegressionTester(const vtkAPVRegressionTester&) = delete;
  void operator=(const vtkAPVRegressionTester&) = delete;
};

// 0.15 threshold is arbitrary but found to
// allow most graphics system variances to pass
// when they should and fail when they should
#define vtkAPVRegressionTestImage(view, pvapp, rlp)                                                \
  vtkAPVRegressionTester::Test(argc, argv, view, pvapp, rlp, 0.15)

#define vtkAPVRegressionTestImageThreshold(view, pvapp, rlp, t)                                    \
  vtkAPVRegressionTester::Test(argc, argv, view, pvapp, rlp, t)

#endif // vtkAPVRegressionTestImage_h
// VTK-HeaderTest-Exclude: vtkAPVRegressionTestImage.h
