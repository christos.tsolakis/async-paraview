/*=========================================================================

  Program:   ParaView
  Module:    vtkAPVRegressionTestImage.cxx

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

#include "vtkAPVRegressionTestImage.h"

#include "vtkCallbackCommand.h"
#include "vtkCompressedVideoPacket.h"
#include "vtkObjectFactory.h"
#include "vtkPVApplicationOptions.h"
#include "vtkPVCoreApplication.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSMPropertyHelper.h"
#include "vtkSMViewProxy.h"

//----------------------------------------------------------------------------
namespace
{
class ScopedPropertyRollback
{
public:
  ScopedPropertyRollback(vtkSMProxy* proxy, const std::string& propertyName, int newValue)
    : Proxy(proxy)
    , PropertyName(propertyName)
  {
    vtkSMPropertyHelper(proxy, propertyName.c_str()).Set(newValue);
    this->OldValue = vtkSMPropertyHelper(proxy, propertyName.c_str()).GetAsInt(0);
  }
  ~ScopedPropertyRollback()
  {
    vtkSMPropertyHelper(this->Proxy, this->PropertyName.c_str()).Set(this->OldValue);
  }

private:
  vtkSMProxy* Proxy = nullptr;
  std::string PropertyName;
  int OldValue = 0;
};
} // end anonymous namespace

//----------------------------------------------------------------------------
vtkStandardNewMacro(vtkAPVRegressionTester);

//----------------------------------------------------------------------------
vtkAPVRegressionTester::vtkAPVRegressionTester() = default;

//----------------------------------------------------------------------------
vtkAPVRegressionTester::~vtkAPVRegressionTester() = default;

//----------------------------------------------------------------------------
void vtkAPVRegressionTester::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

//----------------------------------------------------------------------------
rxcpp::observable<bool> vtkAPVRegressionTester::RenderAndCaptureView(vtkSMViewProxy* view)
{
  // Enable view streaming so that we can process events until one frame is rendered.
  ScopedPropertyRollback viewPropertySaver(view, "StreamOutput", 1);
  return view->Update()
    .combine_latest(view->GetViewOutputObservable())
    .take(1)
    .map([](std::tuple<bool, vtkSmartPointer<vtkCompressedVideoPacket>> tup) {
      return std::get<0>(tup) && (std::get<1>(tup) != nullptr);
    });
}

namespace
{
struct CallbackBridgeData
{
  vtkPVCoreApplication* Application = nullptr;
  vtkrxcpp::schedulers::run_loop* RunLoop = nullptr;
};
void TriggerProcessEvents(vtkObject*, unsigned long eventId, void* clientdata, void*)
{
  if (auto bridge = reinterpret_cast<CallbackBridgeData*>(clientdata))
  {
    vtkLogScopeF(TRACE, "triggerEventId: %lu", eventId);
    bridge->Application->ProcessEvents(*(bridge->RunLoop));
  }
}
}

//----------------------------------------------------------------------------
int vtkAPVRegressionTester::Test(int argc, char* argv[], vtkSMViewProxy* view,
  vtkPVCoreApplication* pvapp, vtkrxcpp::schedulers::run_loop& rlp, double threshold)
{
  // Process events until view is updated AND something is displayed.
  pvapp->Await(rlp, vtkAPVRegressionTester::RenderAndCaptureView(view));
  // Interact with view if needed.
  int retVal = vtkAPVRegressionTester::Test(argc, argv, view, pvapp, threshold);
  if (retVal == vtkTesting::DO_INTERACTOR)
  {
    auto iren = view->GetRenderWindowInteractor();
    vtkNew<vtkCallbackCommand> triggerProcessEvents;
    CallbackBridgeData clientData{ pvapp, &rlp };
    triggerProcessEvents->SetClientData(&clientData);
    triggerProcessEvents->SetCallback(::TriggerProcessEvents);
    // Dispatch and execute rxcpp schedulables for smooth interaction.
    iren->AddObserver(vtkCommand::ConfigureEvent, triggerProcessEvents);
    iren->AddObserver(vtkCommand::WindowResizeEvent, triggerProcessEvents);
    iren->AddObserver(vtkCommand::RenderEvent, triggerProcessEvents);
    // Start the VTK interactor event loop, runs indefinitely until window is closed.
    iren->Start();
    retVal = vtkTesting::PASSED;
  }
  return retVal;
}

//----------------------------------------------------------------------------
int vtkAPVRegressionTester::Test(
  int argc, char* argv[], vtkSMViewProxy* view, vtkPVCoreApplication* pvapp, double threshold)
{
  // pass command line args to vtkTesting
  int retVal = vtkTesting::NOT_RUN;
  vtkNew<vtkTesting> testing;
  for (int i = 0; i < argc; ++i)
  {
    testing->AddArgument(argv[i]);
  }

  // translate vtkPVApplicationOptions style arguments to vtkTesting
  if (auto options = vtkPVApplicationOptions::SafeDownCast(pvapp->GetOptions()))
  {
    // smTestDriver provides baseline image using "--test-baseline" "/path/to/baseline.png"
    // vtkTesting expects baseline image using "-V" "/path/to/baseline.png"
    auto baseline = options->GetBaselineFilepath();
    if (!baseline.empty())
    {
      testing->AddArgument("-V");
      testing->AddArgument(baseline.c_str());
    }
    // smTestDriver provides test directory using "--test-directory"
    // "/path/to/Module/Testing/Temporary" vtkTesting expects test directory using "-V"
    // "/path/to/Module/Testing/Temporary"
    auto testDir = options->GetTestDirectory();
    if (!testDir.empty())
    {
      testing->AddArgument("-T");
      testing->AddArgument(testDir.c_str());
    }
  }
  if (testing->IsInteractiveModeSpecified())
  {
    return vtkTesting::DO_INTERACTOR;
  }
  // Perform regression test against a baseline image if needed.
  if (testing->IsValidImageSpecified())
  {
    testing->SetRenderWindow(view->GetRenderWindow());
    retVal = testing->RegressionTestAndCaptureOutput(threshold, cout);
  }
  return retVal;
}

//----------------------------------------------------------------------------
int vtkAPVRegressionTester::Test(std::vector<std::string> args, vtkSMViewProxy* view,
  vtkPVCoreApplication* pvapp, double threshold)
{
  std::vector<char*> argv(args.size(), nullptr);
  for (int i = 0; i < args.size(); ++i)
  {
    argv[i] = &args[i].front();
  }
  return vtkAPVRegressionTester::Test(
    static_cast<int>(argv.size()), argv.data(), view, pvapp, threshold);
}
