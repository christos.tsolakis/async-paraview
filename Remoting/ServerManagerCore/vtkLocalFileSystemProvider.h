/*=========================================================================

  Program:   ParaView
  Module:    vtkLocalFileSystemProvider.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkLocalFileSystemProvider
 * @brief provider that adds support browsing and modifying a local filesystem
 *
 * In contrast to vtkRemoteFileSystemProvider, vtkLocalFileSystemProvider
 * executes the filesystem commands locally on the calling thread and involves
 * no communication. Although it might look like an overly complex approach for
 * system calls it allows to avoid code duplication be adding new operations
 * to vtkRemoteFileSystemProvider and simplifies the handling of the two cases
 * (local/remote) filesystem in the Microservices layer.
 *
 * @sa vtkRemoteFileSystemProvider
 */

#ifndef vtkLocalFileSystemProvider_h
#define vtkLocalFileSystemProvider_h
#include "vtkRemotingServerManagerCoreModule.h" // for exports

#include "vtkRemoteFileSystemProvider.h"

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class VTKREMOTINGSERVERMANAGERCORE_EXPORT vtkLocalFileSystemProvider
  : public vtkRemoteFileSystemProvider
{
public:
  static vtkLocalFileSystemProvider* New();
  vtkTypeMacro(vtkLocalFileSystemProvider, vtkRemoteFileSystemProvider);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  // Process the packet locally and return a single value observable with the resulting packet
  virtual rxcpp::observable<vtkPacket> ProcessLocally(const vtkPacket& packet);

protected:
  vtkLocalFileSystemProvider();
  ~vtkLocalFileSystemProvider() override;

  void InitializeInternal(vtkService* service) override;

private:
  vtkLocalFileSystemProvider(const vtkLocalFileSystemProvider&) = delete;
  void operator=(const vtkLocalFileSystemProvider&) = delete;
};

#endif
