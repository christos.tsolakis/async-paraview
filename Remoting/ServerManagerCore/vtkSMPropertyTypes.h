/*=========================================================================

  Program:   ParaView
  Module:    vtkSMProperty.h

  Copyright (c) Kitware, Inc.
  All rights reserved.
  See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
#ifndef vtkSMPropertyTypes_h
#define vtkSMPropertyTypes_h
#include "vtkRemotingServerManagerCoreModule.h"

#include <set>
#include <string>

class VTKREMOTINGSERVERMANAGERCORE_EXPORT vtkSMPropertyTypes
{
public:
  // Please add new property types above MAX_PROPERTY_TYPE_COUNT.
  enum PropertyTypes
  {
    INVALID,
    INT,
    DOUBLE,
    ID_TYPE,
    STRING,
    PROXY,
    INPUT,
    MAX_PROPERTY_TYPE_COUNT
  };

  //---------------------------------------------------------------------------
  static std::set<std::string> GetPropertyClassNames()
  {
    std::set<std::string> classNames;
    int maxPropertyTypes = static_cast<int>(PropertyTypes::MAX_PROPERTY_TYPE_COUNT);
    for (int i = 0; i < maxPropertyTypes; ++i)
    {
      const auto propertyType = static_cast<PropertyTypes>(i);
      const auto propertyName = vtkSMPropertyTypes::GetPropertyClassName(propertyType);
      classNames.insert(propertyName);
    }
    return classNames;
  }

  //---------------------------------------------------------------------------
  static std::string GetPropertyClassName(const PropertyTypes& propertyType)
  {
    switch (propertyType)
    {
      case INT:
        return "vtkSMIntVectorProperty";
      case DOUBLE:
        return "vtkSMDoubleVectorProperty";
      case ID_TYPE:
        return "vtkSMIdTypeVectorProperty";
      case STRING:
        return "vtkSMStringVectorProperty";
      case PROXY:
        return "vtkSMProxyProperty";
      case INPUT:
        return "vtkSMInputProperty";
      default:
        return "";
    }
  }

  //---------------------------------------------------------------------------
  static int GetPropertyType(const std::string& className)
  {
    if (className == "vtkSMIntVectorProperty")
    {
      return INT;
    }
    if (className == "vtkSMDoubleVectorProperty")
    {
      return DOUBLE;
    }
    if (className == "vtkSMIdTypeVectorProperty")
    {
      return ID_TYPE;
    }
    if (className == "vtkSMStringVectorProperty")
    {
      return STRING;
    }
    if (className == "vtkSMProxyProperty")
    {
      return PROXY;
    }
    if (className == "vtkSMInputProperty")
    {
      return INPUT;
    }

    return INVALID;
  }
};

#endif
