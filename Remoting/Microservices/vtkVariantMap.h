/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkVariantMap.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkVariantMap
 *
 * vtkVariantMap can be used to map python's dict to a vtkObject which is
 * Python-wrappable and thus allows to move easily information between
 * Python<->C++ in the context of proxy manipulation.
 */
#ifndef vtkVariantMap_h
#define vtkVariantMap_h
#include "vtkRemotingMicroservicesModule.h" // for exports
#include <map>
#include <memory>
#include <vtkVariant.h>
class VTKREMOTINGMICROSERVICES_EXPORT vtkVariantMap : public vtkObject
{
public:
  static vtkVariantMap* New();
  vtkTypeMacro(vtkVariantMap, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  void Set(const std::string& key, const vtkVariant& value);
  vtkVariant Get(const std::string& key) const;

  std::map<std::string, vtkVariant>::iterator begin();
  std::map<std::string, vtkVariant>::iterator end();
  std::map<std::string, vtkVariant>::const_iterator begin() const;
  std::map<std::string, vtkVariant>::const_iterator end() const;

protected:
  vtkVariantMap();
  ~vtkVariantMap() override;

private:
  vtkVariantMap(const vtkVariantMap&) = delete;
  void operator=(const vtkVariantMap&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};
#endif
