/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkDefinitionManagerMicroservice.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * @class vtkDefinitionManagerMicroservice
 * @brief A class to track definition of a proxy
 *
 *
 */

#ifndef vtkDefinitionManagerMicroservice_h
#define vtkDefinitionManagerMicroservice_h

#include "vtkObject.h"
#include "vtkPVXMLElement.h" // due to vtkPythonObservableWrapper limitations full definition is required
#include "vtkPropertyWidgetDecorator.h"
#include "vtkProxyAdapter.h" // due to vtkPythonObservableWrapper limitations full definition is required
#include "vtkPythonObservableWrapper.h"     // for VTK_REMOTING_MAKE_PYTHON_OBSERVABLE
#include "vtkRemotingMicroservicesModule.h" // for exports
#include "vtkSmartPointer.h"                // for vtkSmartPointer

#include <memory> // for std::unique_ptr

#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
// ideally, we include rx-lite.hpp here.
#include VTK_REMOTING_RXCPP(rx.hpp)
// clang-format on

class vtkClientSession;

class VTKREMOTINGMICROSERVICES_EXPORT vtkDefinitionManagerMicroservice : public vtkObject
{
public:
  static vtkDefinitionManagerMicroservice* New();
  vtkTypeMacro(vtkDefinitionManagerMicroservice, vtkObject);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  ///@{
  /**
   * Get the most current definition for the given proxy
   *
   * Throws std::runtime_error if session is not set.
   */

  vtkSmartPointer<vtkProxyAdapter> GetDefinition(vtkSMProxy* smproxy) const;
  vtkSmartPointer<vtkProxyAdapter> GetDefinition(
    const std::string& group, const std::string& name) const;

  ///@}

  ///@{
  /**
   * Get the most current xml layout for the given proxy
   *
   * Throws std::runtime_error if session is not set.
   */
  vtkSmartPointer<vtkPVXMLElement> GetLayout(vtkSMProxy* smproxy) const;
  vtkSmartPointer<vtkPVXMLElement> GetLayout(
    const std::string& group, const std::string& name) const;
  ///@}

  std::vector<vtkSmartPointer<vtkPropertyWidgetDecorator>> GetPropertyDecorators(
    vtkSMProxy* proxy, const std::string& propertyName) const;

  std::vector<vtkSmartPointer<vtkPropertyWidgetDecorator>> GetPropertyGroupDecorators(
    vtkSMProxy* proxy, const std::string& propertyGroup) const;

  using PrototypesT = std::vector<vtkSmartPointer<vtkSMProxy>>;

  PrototypesT GetCompatibleDefinitions(
    const std::vector<vtkSmartPointer<vtkSMProxy>>& smproxies, const std::string& groupName) const;

  ///@{
  /**
   * Returns an observable for all the definitions.
   *
   * The return value is a cold observable i.e. it does not start emitting
   * values until something subscribes to it.
   *
   * @section Triggers Triggers
   *
   * * on_next: every time the session is changed
   *
   * * on_error: if no session is set
   *
   * * on_completed: when this instance is destroyed or UnsubscribeAll is called.
   *
   * @todo needs to fire on_next eveyr time a new definition is added.
   */
  rxcpp::observable<PrototypesT> GetDefinitionsObservable() const;
  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(
    std::vector<vtkSmartPointer<vtkObject>>, GetDefinitionsObservable() const);

  rxcpp::observable<PrototypesT> GetDefinitionsObservable(const std::string& groupName) const;

  VTK_REMOTING_MAKE_PYTHON_OBSERVABLE(std::vector<vtkSmartPointer<vtkObject>>,
    GetDefinitionsObservable(const std::string& groupName) const);
  ///@}

  /**
   * Emit a completion signal to all observables created through
   * `GetSelectionObservable()` and `GetCurrentObservable()`. This will cause
   * all the subscriptions to complete. In Python API all async iterators will
   * also complete.
   */
  void UnsubscribeAll();

  ///@{
  /**
   * Get/set the session.
   */
  void SetSession(vtkClientSession* session);
  vtkClientSession* GetSession() const;
  ///@}

protected:
  vtkDefinitionManagerMicroservice();
  ~vtkDefinitionManagerMicroservice() override;

private:
  vtkDefinitionManagerMicroservice(const vtkDefinitionManagerMicroservice&) = delete;
  void operator=(const vtkDefinitionManagerMicroservice&) = delete;

  class vtkInternals;
  std::unique_ptr<vtkInternals> Internals;
};

#endif
