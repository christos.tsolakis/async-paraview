#include "vtkStateMicroservice.h"
#include "vtkClientSession.h"
#include "vtkObjectFactory.h"
#include "vtkRemotingCoreUtilities.h"
#include "vtkSMParaViewPipelineController.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMState.h"

#include <memory>
#include <stdexcept>
#include <thread>

class vtkStateMicroservice::vtkInternals
{
public:
  const std::thread::id OwnerTID{ std::this_thread::get_id() };
  vtkSmartPointer<vtkClientSession> Session;

  bool IsInitialized() const { return Session != nullptr; }
};

//-----------------------------------------------------------------------------
vtkStandardNewMacro(vtkStateMicroservice);

//-----------------------------------------------------------------------------
vtkStateMicroservice::vtkStateMicroservice()
  : Internals(std::make_unique<vtkStateMicroservice::vtkInternals>())
{
}

//-----------------------------------------------------------------------------
vtkStateMicroservice::~vtkStateMicroservice() = default;

//-----------------------------------------------------------------------------
void vtkStateMicroservice::SetSession(vtkClientSession* session)
{
  auto& internals = *(this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  if (internals.Session != session)
  {
    vtkNew<vtkSMParaViewPipelineController> controller;
    bool status = controller->InitializeSession(session);
    internals.Session = status ? session : nullptr;
    if (!status)
    {
      vtkLog(ERROR, << "vtkSMParaViewPipelineController failed to initialize session.");
    }
  }
}

//-----------------------------------------------------------------------------
vtkClientSession* vtkStateMicroservice::GetSession() const
{
  const auto& internals = *(this->Internals);
  vtkRemotingCoreUtilities::EnsureThread(internals.OwnerTID);
  return internals.Session;
}

//-----------------------------------------------------------------------------
vtkSMState vtkStateMicroservice::Save()
{
  const auto& internals = *(this->Internals);
  if (!internals.IsInitialized())
  {
    throw std::runtime_error("Session was not set!");
  }

  return internals.Session->GetProxyManager()->GetState();
}

//-----------------------------------------------------------------------------
void vtkStateMicroservice::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
