/*=========================================================================

Program:   ParaView
Module:    TestFileSystemMicroserviceRestrictBrowsing.cxx

Copyright (c) Kitware, Inc.
All rights reserved.
See Copyright.txt or http://www.paraview.org/HTML/Copyright.html for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/**
 * Tests vtkFileSystemMicroservice.
 */

#include "vtkClientSession.h"
#include "vtkCollection.h"
#include "vtkDistributedEnvironment.h"
#include "vtkFileSystemMicroservice.h"
#include "vtkLogger.h"
#include "vtkPVApplication.h"
#include "vtkPVApplicationOptions.h"
#include "vtkTestUtilities.h"
#include "vtk_rxcpp.h" // for rxcpp
// clang-format off
#include VTK_REMOTING_RXCPP(rx-util.hpp)
// clang-format on

#include <vtksys/SystemTools.hxx>

#include <cstdlib>
#include <vtk_cli11.h>

#define VALIDATE(x)                                                                                \
  if (!(x))                                                                                        \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x ")...failed!");                                             \
    throw std::runtime_error("Test failed");                                                       \
  }

#define VTK_EXPECT(x, n)                                                                           \
  if (x != n)                                                                                      \
  {                                                                                                \
    vtkLogF(ERROR, "Validating : (" #x " == " #n ")...failed!");                                   \
    throw std::runtime_error("Test failed");                                                       \
  }

namespace
{
std::string JoinPathNative(std::vector<std::string> components)
{

  return vtksys::SystemTools::ConvertToOutputPath(vtksys::SystemTools::JoinPath(components));
}
}

// LABEL created directories to avoid races between client-server and
// builtin-session test cases when run in parallel
std::string FILESYSTEM_RESTRICT_LABEL;
bool DoFileSystemMicroserviceRestrictBrowsingTest(vtkClientSession* session,
  rxcpp::schedulers::run_loop& runLoop, vtkFileSystemMicroservice::Location location)
{

#if defined(_WIN32) && !defined(__CYGWIN__)
  const std::string SEPARATOR = "\\";
#else
  const std::string SEPARATOR = "/";
#endif
  vtkNew<vtkFileSystemMicroservice> fservice;
  auto app = vtkPVApplication::GetInstance();

  const std::string testDirectory =
    vtksys::SystemTools::ConvertToOutputPath(app->GetOptions()->GetTestDirectory());
  // Split path into its constituent components so we can reuse it to compose testing paths
  std::vector<std::string> directoryPathComponents;
  vtksys::SystemTools::SplitPath(testDirectory, directoryPathComponents);
  directoryPathComponents.push_back(FILESYSTEM_RESTRICT_LABEL);
  const std::string baseDirectory = JoinPathNative(directoryPathComponents);
  vtkLogF(INFO, "%s", baseDirectory.c_str());
  vtkLogF(INFO, "%s", testDirectory.c_str());

  fservice->SetSession(session);

  // // Let's create: ....
  // Top
  // | - Allowed
  //      | - File
  // | - Not Allowed
  //      | - File

  int errorsCaught = 0;

  std::vector<std::string> basePath = directoryPathComponents;
  basePath.push_back("TestTopDir");

  std::vector<std::string> allowedPath = basePath;
  allowedPath.push_back("Allowed");

  std::vector<std::string> notAllowedPath = basePath;
  notAllowedPath.push_back("NotAllowed");

  bool makeDirStatus;

  const std::string topDirectory = JoinPathNative(basePath);
  makeDirStatus = app->Await(runLoop, fservice->MakeDirectory(topDirectory, location));
  VALIDATE(makeDirStatus == true);

  const std::string allowedDirectory = JoinPathNative(allowedPath);
  makeDirStatus = app->Await(runLoop, fservice->MakeDirectory(allowedDirectory, location));
  VALIDATE(makeDirStatus == true);

  const std::string notAllowedDirectory = JoinPathNative(notAllowedPath);
  makeDirStatus = app->Await(runLoop, fservice->MakeDirectory(notAllowedDirectory, location));
  VALIDATE(makeDirStatus == true);

  // create dummy file
  vtksys::Status status;
  std::vector<std::string> allowedFilePath = allowedPath;
  allowedFilePath.push_back("allowed.txt");
  std::string allowedFile = JoinPathNative(allowedFilePath);
  status = vtksys::SystemTools::Touch(allowedFile, /* create */ true);
  VALIDATE(status.IsSuccess());

  std::vector<std::string> notAllowedFilePath = notAllowedPath;
  notAllowedFilePath.push_back("notAllowed.txt");
  std::string notAllowedFile = JoinPathNative(notAllowedFilePath);
  status = vtksys::SystemTools::Touch(notAllowedFile, /* create */ true);
  VALIDATE(status.IsSuccess());

  //------------------------------------------------------------------------
  // Initially both directories can be listed
  vtkSmartPointer<vtkPVFileInformation> fileInfo;
  fileInfo = app->Await(runLoop, fservice->ListDirectory(allowedDirectory, location));
  // fileInfo->Print(std::cout);
  VTK_EXPECT(fileInfo->GetContents()->GetNumberOfItems(), 1);
  fileInfo = app->Await(runLoop, fservice->ListDirectory(notAllowedDirectory, location));
  VTK_EXPECT(fileInfo->GetContents()->GetNumberOfItems(), 1);
  // fileInfo->Print(std::cout);

  // restrict to allowed and map to app-data
  const std::string mappedDirectory = SEPARATOR + "app-data";
  app->Await(runLoop, fservice->AddRootDirectory(mappedDirectory, allowedDirectory, location));
  const std::string mappedDirectory2 = SEPARATOR + "app-data2";
  app->Await(runLoop, fservice->AddRootDirectory(mappedDirectory2, allowedDirectory, location));

  // both should fail we only know about mapped paths now.
  try
  {
    app->Await(runLoop, fservice->ListDirectory(allowedDirectory, location));
    VALIDATE(false);
  }
  catch (std::runtime_error& ep)
  {
    errorsCaught++;
    vtkLogF(INFO, "%s", ep.what());
  }
  try
  {
    app->Await(runLoop, fservice->ListDirectory(notAllowedDirectory, location));
    VALIDATE(false);
  }
  catch (std::runtime_error& ep)
  {
    errorsCaught++;
    vtkLogF(INFO, "%s", ep.what());
  }

  fileInfo = app->Await(runLoop, fservice->ListDirectory(mappedDirectory, location));
  // fileInfo->Print(std::cout);
  VTK_EXPECT(fileInfo->GetContents()->GetNumberOfItems(), 1);
  // make sure mapping works for top level directory
  VTK_EXPECT(std::string(fileInfo->GetFullPath()), mappedDirectory);

  // make sure mapping works for included file
  vtkPVFileInformation* objectInfo =
    vtkPVFileInformation::SafeDownCast(fileInfo->GetContents()->GetItemAsObject(0));
  VALIDATE(objectInfo);
  // objectInfo->Print(std::cout);

  std::vector<std::string> mappedFilePath = { SEPARATOR, "app-data", "allowed.txt" };
  std::string mappedFile = JoinPathNative(mappedFilePath);
  vtkLogF(INFO, "%s", mappedFile.c_str());
  VTK_EXPECT(std::string(objectInfo->GetFullPath()), mappedFile);

  // attempt to do other operations that involve forbidden locations

  std::vector<std::string> notAllowedFilePath2 = notAllowedPath;
  notAllowedFilePath2.push_back("notAllowed2.txt");
  std::string notAllowedFile2 = JoinPathNative(notAllowedFilePath2);

  try
  {
    app->Await(runLoop, fservice->Rename(notAllowedFile, notAllowedFile2, location));
    VALIDATE(false);
  }
  catch (std::runtime_error& ep)
  {
    errorsCaught++;
    vtkLogF(INFO, "%s", ep.what());
  }

  try
  {
    app->Await(runLoop, fservice->Rename(mappedFile, notAllowedFile2, location));
    VALIDATE(false);
  }
  catch (std::runtime_error& ep)
  {
    errorsCaught++;
    vtkLogF(INFO, "%s", ep.what());
  }

  try
  {
    app->Await(runLoop, fservice->Remove(notAllowedFile2, location));
    VALIDATE(false);
  }
  catch (std::runtime_error& ep)
  {
    errorsCaught++;
    vtkLogF(INFO, "%s", ep.what());
  }

  bool file_removed = app->Await(runLoop, fservice->Remove(mappedFile, location));
  VALIDATE(file_removed);

  // root should map to all mapped directories
  fileInfo = app->Await(runLoop, fservice->ListDirectory(SEPARATOR, location));
  // fileInfo->Print(std::cout);
  VTK_EXPECT(fileInfo->GetContents()->GetNumberOfItems(), 2);
  // make sure the children are the expected
  vtkPVFileInformation* objectInfo0 =
    vtkPVFileInformation::SafeDownCast(fileInfo->GetContents()->GetItemAsObject(0));
  VTK_EXPECT(std::string(objectInfo0->GetFullPath()), SEPARATOR + "app-data");
  vtkPVFileInformation* objectInfo1 =
    vtkPVFileInformation::SafeDownCast(fileInfo->GetContents()->GetItemAsObject(1));
  VTK_EXPECT(std::string(objectInfo1->GetFullPath()), SEPARATOR + "app-data2");

  VALIDATE(app->Await(runLoop, fservice->RemoveRootDirectory(SEPARATOR + "app-data", location)));
  VALIDATE(app->Await(runLoop, fservice->RemoveRootDirectory(SEPARATOR + "app-data2", location)));

  // After removing the restriction operations should succeed
  VALIDATE(app->Await(runLoop, fservice->ListDirectory(allowedDirectory, location)));
  VALIDATE(app->Await(runLoop, fservice->ListDirectory(notAllowedDirectory, location)));
  VALIDATE(app->Await(runLoop, fservice->Rename(notAllowedFile, notAllowedFile2, location)));
  VALIDATE(app->Await(runLoop, fservice->Remove(notAllowedFile2, location)));
  VTK_EXPECT(errorsCaught, 5);
  // Clean up
  VALIDATE(app->Await(runLoop, fservice->Remove(baseDirectory, location)));
  return true;
}

int TestFileSystemMicroserviceRestrictBrowsing(int argc, char* argv[])
{
  // Create the primary event-loop for the application.
  rxcpp::schedulers::run_loop runLoop;

  // initialize the MPI environment.
  const vtkDistributedEnvironment environment(&argc, &argv);
  const int rank = environment.GetRank();
  const int nranks = environment.GetNumberOfRanks();

  // determine what mode this test is running under
  // and create appropriate application type.
  vtkNew<vtkPVApplication> app;

  // process command line arguments.
  CLI::App cli("TestRemoteFileService browsing restrition");

  // let's use nicer formatter for help text.
  vtkPVApplicationOptions::SetupDefaults(cli);

  app->GetOptions()->Populate(cli);

  try
  {
    cli.parse(argc, argv);
  }
  catch (const CLI::ParseError& e)
  {
    return (rank == 0 ? cli.exit(e) : EXIT_SUCCESS);
  }

  app->Initialize(argv[0], rxcpp::observe_on_run_loop(runLoop), environment.GetController());
  rxcpp::observable<vtkTypeUInt32> sessionIdObservable;

  auto* options = app->GetOptions();

  FILESYSTEM_RESTRICT_LABEL = "TestFileSystemMicroserviceRestrictBrowsing";
  if (options->GetServerURL().empty())
  {
    sessionIdObservable = app->CreateBuiltinSession();
    FILESYSTEM_RESTRICT_LABEL += "_builtin_";
  }
  else
  {
    sessionIdObservable = app->CreateRemoteSession(options->GetServerURL());
    // client-server could run in two flavors with and without MPI, use port number to make sure we
    // do step on each other
    FILESYSTEM_RESTRICT_LABEL +=
      "_client-server_" + vtksys::SystemTools::SplitString(options->GetServerURL(), ':')[2];
  }

  vtkTypeUInt32 sessionId;

  sessionIdObservable.subscribe(
    [&](vtkTypeUInt32 id) {
      if (id == 0)
      {
        app->Exit(EXIT_FAILURE);
      }
      else
      {
        sessionId = id;
        bool success = DoFileSystemMicroserviceRestrictBrowsingTest(
          app->GetSession(id), runLoop, vtkFileSystemMicroservice::Location::DATA_SERVICE);
        success &= DoFileSystemMicroserviceRestrictBrowsingTest(
          app->GetSession(id), runLoop, vtkFileSystemMicroservice::Location::RENDER_SERVICE);
        success &= DoFileSystemMicroserviceRestrictBrowsingTest(
          app->GetSession(id), runLoop, vtkFileSystemMicroservice::Location::CLIENT);
        app->Await(
          runLoop, app->GetSession(id)->Disconnect(/*exit_server=*/options->GetServerExit()));
        app->Exit(success ? EXIT_SUCCESS : EXIT_FAILURE);
      }
    },
    // on_error
    [&](std::exception_ptr error_ptr) {
      vtkLogF(ERROR, "%s", rxcpp::util::what(error_ptr).c_str());
      app->Await(
        runLoop, app->GetSession(sessionId)->Disconnect(/*exit_server=*/options->GetServerExit()));
      app->Exit(EXIT_FAILURE);
    });
  return app->Run(runLoop);
}
