import async_paraview.modules.vtkRemotingMicroservices
from async_paraview.modules.vtkRemotingServerManager import vtkClientSession
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)


class ActiveObjects(
    async_paraview.modules.vtkRemotingMicroservices.vtkActiveObjectsMicroservice
):
    """
    A thin Python Wrapper around vtkActiveObjectsMicroservice.
    """

    def __init__(self, session=None, modelName=None):

        if session is not None:
            if not isinstance(session, vtkClientSession):
                raise TypeError(" 'session' argument is not of type vtkClientSession")

            super().SetSession(session)

        if modelName is not None:
            if not isinstance(modelName, str):
                raise TypeError(" 'modelName' argument is not of type str")

            super().SetModelName(modelName)

    def GetCurrentObservable(self):
        """Get an async iterator that corresponds to the values of the observable

        Example:

        # Prints the current proxy every time there is an update.
        async for value in activeObjectsObserver.GetCurrentObservable():
            if value:
                print(value.GetXMLName())
            else:
                observed.append(None)
        """
        return vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetCurrentObservable()
        )

    def GetSelectionObservable(self):
        """Get an async iterator that corresponds to the values of the SelectionObservable.

        GetSelectionObservable() -> list[vtkSMProxy]
        """
        return vtkPythonObservableWrapperUtilities.GetIterator(
            super().GetSelectionObservable()
        )
