# see paraview-visualizer/pv_visualizer/app/engine/proxymanager/domain_helpers.py
import logging
from trame_simput.core.domains import PropertyDomain, register_property_domain
from async_paraview.modules.vtkRemotingServerManager import vtkSMDomain
from async_paraview.services import definitions


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# -----------------------------------------------------------------------------
# General util functions
# -----------------------------------------------------------------------------

APV_PXM = None


def ensure_pxm():
    global APV_PXM
    if APV_PXM is None:
        from .simput import PVS

        APV_PXM = PVS


def id_pv_to_simput(pv_id):
    if APV_PXM is None:
        ensure_pxm()
    logger.debug(pv_id)
    logger.debug(APV_PXM.to_sinput(pv_id))
    return APV_PXM.to_sinput(pv_id)


# -----------------------------------------------------------------------------
# Generic ParaView domain adapter
# -----------------------------------------------------------------------------
import sys


def domain_range(domain):
    if domain.GetClassName() == "vtkSMDoubleRangeDomain":
        _max = sys.float_info.max
    else:
        _max = sys.maxsize

    level = 0
    value_range = [-_max, _max]

    if domain.GetMinimumExists(0):
        value_range[0] = domain.GetMinimum(0)
        level += 1

    if domain.GetMaximumExists(0):
        value_range[1] = domain.GetMaximum(0)
        level += 1

    return value_range


def domain_enum(domain):
    values = []
    for i in range(domain.GetNumberOfEntries()):
        values.append(
            {"text": domain.GetEntryText(i), "value": domain.GetEntryValue(i)}
        )
    return values


def domain_scalar_range(domain):
    values = []
    for i in range(domain.GetNumberOfEntries()):
        values.append(
            {"text": domain.GetEntryText(i), "value": domain.GetEntryValue(i)}
        )
    return values


# -----------------------------------------------------------------------------
def domain_list_strings(domain):
    size = domain.GetNumberOfStrings()
    values = []
    for i in range(size):
        values.append(
            {
                "text": domain.GetString(i),
                "value": domain.GetString(i),
            }
        )
    return values


# -----------------------------------------------------------------------------
def domain_list_arrays(domain):
    # proxy = domain.GetProperty().GetParent()
    data_info = None
    # if proxy.IsA("vtkSMRepresentationProxy"):
    #    data_info = proxy.GetRepresentedDataInformation()
    result_list = []
    property_size = domain.GetProperty().GetNumberOfElements()

    def _value(association, name):
        if property_size == 5:
            return [
                "0",
                "0",
                "0",
                f"{association}",
                name,
            ]
        elif property_size == 1:
            return [name]
        elif property_size == 2:
            return [
                f"{association}",
                name,
            ]
        else:
            raise RuntimeError(
                f"Invalid size for propery of Arraylist Domain: '{property_size}' or property {domain.GetProperty().GetXMLName()} expected 2 or 5"
            )

    for idx, entry in enumerate(domain_list_strings(domain)):
        label = entry.get("text")
        name = entry.get("value")
        association = domain.GetFieldAssociation(idx)
        entry = {"text": label, "value": _value(association, name)}
        if data_info:
            components_list = []
            array_info = data_info.GetArrayInformation(label, association)
            if array_info:
                nb_components = array_info.GetNumberOfComponents()
                if nb_components > 1:
                    components_list.append(array_info.GetComponentName(-1))
                    for i in range(nb_components):
                        components_list.append(array_info.GetComponentName(i))
                    entry["components"] = components_list

        result_list.append(entry)

    return result_list


# -----------------------------------------------------------------------------
def domain_list_proxies(domain):
    size = domain.GetNumberOfProxies()
    values = []
    for i in range(size):
        proxy = domain.GetProxy(i)
        values.append(
            {
                "text": proxy.GetXMLLabel(),
                "value": str(proxy.GetGlobalID()),
            }
        )
    return values


# -----------------------------------------------------------------------------
def domain_list_proxies_simput_ids(domain):
    logger.debug("Entries")
    for entry in domain_list_proxies(domain):
        logger.debug(f"{entry}")

    return [
        {
            "text": entry.get("text"),
            "value": id_pv_to_simput(entry.get("value")).id,
        }
        for entry in domain_list_proxies(domain)
    ]


# -----------------------------------------------------------------------------
def domain_bool(domain):
    return {}


DOMAIN_AVAILABLE_VALUES = {
    "vtkSMDoubleRangeDomain": domain_range,
    "vtkSMBoundsDomain": domain_range,
    "vtkSMIntRangeDomain": domain_range,
    "vtkSMEnumerationDomain": domain_enum,
    "vtkSMArrayListDomain": domain_list_arrays,
    "vtkSMArrayRangeDomain": domain_range,
    "vtkSMBooleanDomain": domain_bool,
    "vtkSMProxyListDomain": domain_list_proxies_simput_ids,
    "vtkSMStringListDomain": domain_list_strings,
}

UNKNOWN_DOMAINS = set()


def domain_unknown(domain):
    class_name = domain.GetClassName()

    if class_name not in UNKNOWN_DOMAINS:
        UNKNOWN_DOMAINS.add(class_name)
        logger.info("domain_unknown::class(%s)", class_name)

    return {}


# -----------------------------------------------------------------------------


class ParaViewDomain(PropertyDomain):
    """
    ParaViewDomain connects the servermanager property domain to the simput domain notion.

    In this implementation we expose the C++ inner workings through "available" and "valid" methods.

    Parameters
    ----------
    _proxy : trame_simput.core.proxy.Proxy
    _property: str
    kwargs: dict(str,str)
      Example
        {'name': 'bool', 'pv_class': 'vtkSMBooleanDomain', 'pv_name': 'bool'}

    https://github.com/Kitware/trame-simput/blob/master/docs/definitions.md#domain-definitions
    """

    def __init__(self, _proxy, _property, **kwargs):
        super().__init__(_proxy, _property, **kwargs)
        self._pv_proxy = _proxy.object
        self._pv_property = self._pv_proxy.GetProperty(_property)
        self._pv_class = kwargs.get("pv_class")
        self._pv_name = kwargs.get("pv_name")
        self._pv_domain = None
        self._level = 2
        self._available = domain_unknown

        if self._pv_property is None:
            logger.error(
                f"!> No property {_property} on proxy {self._pv_proxy.GetXMLName()}"
            )
            logger.error("~" * 80)
            return

        # Find PV domain instance using the provided pv_class and pv_name
        iter = self._pv_property.NewDomainIterator()
        iter.UnRegister(None)
        iter.Begin()
        while not iter.IsAtEnd():
            domain = iter.GetDomain()
            domain_class = domain.GetClassName()
            domain_name = domain.GetXMLName()

            if self._pv_class == domain_class and self._pv_name == domain_name:
                self._pv_domain = domain
            iter.Next()

        # TODO customize message for each domain
        self._message = kwargs.get(
            "message", f"{_property}>{self._pv_class}::{self._pv_name}"
        )

        # make sure proxies of proxylistdomain have been registered with simput.
        if self._pv_domain.IsA("vtkSMProxyListDomain"):
            logger.debug(
                f"Initialize proxylist for property {self._pv_property.GetXMLName()} of {self._pv_name}"
            )
            ensure_pxm()
            for i in range(self._pv_domain.GetNumberOfProxies()):
                proxy = self._pv_domain.GetProxy(i)
                APV_PXM.pv_proxy_ensure_binding(proxy)

        self._available = DOMAIN_AVAILABLE_VALUES.get(domain_class, domain_unknown)

    def set_value(self):
        return False

    def available(self):
        values = self._available(self._pv_domain)
        logger.debug(f"{self._pv_class}::{self._pv_name} %s", str(values))
        return values

    def valid(self, required_level=2):
        if self._level < required_level:
            return True
        if self._pv_domain is None:
            return True
        # if type(self.value) is list:
        #    for i in range(self._pv_property.GetNumberOfElements()):
        #        self._pv_property.SetUncheckedElement(i,self.value[i]) # TODO
        # else:
        #    self._pv_property.SetUncheckedElement(0,self.value) # TODO
        flag = self._pv_domain.IsInDomain(self._pv_property)

        logger.debug(
            f" Domain Value {self.value} / unchecked property value for {self._pv_property.GetXMLName()} : is accepted ? {flag}"
        )
        logger.debug(
            f" Domain Value {self.value} for {self._pv_property.GetXMLName()} : is accepted ? {flag}"
        )
        if flag == vtkSMDomain.IN_DOMAIN:
            return True
        else:
            return False


# -----------------------------------------------------------------------------

DEFINITION_MANAGER = None


def ensure_defininition_manager(session):
    global DEFINITION_MANAGER
    if DEFINITION_MANAGER is None or DEFINITION_MANAGER.GetSession() != session:
        DEFINITION_MANAGER = definitions.DefinitionManager(session)


class ParaViewDecoratorDomain(PropertyDomain):
    def __init__(self, _proxy, _property, **kwargs):
        super().__init__(_proxy, _property, **kwargs)
        self._pv_proxy = _proxy.object
        self._pv_property = self._pv_proxy.GetProperty(_property)
        self._pv_name = kwargs.get("pv_name")
        self._level = 0
        self._property = _property
        self._decorator = None

        ensure_defininition_manager(self._pv_proxy.GetSession())
        decorators = DEFINITION_MANAGER.GetPropertyDecorators(self._pv_proxy, _property)
        for decorator in decorators:
            if decorator.GetDecoratorType() == self._pv_name:
                self._decorator = decorator
                break
        if self._decorator is None:
            logger.error(
                f"ERROR could not find decorator of class vtk{self._pv_name} in {decorators}"
            )

    def set_value(self):
        logger.debug(f" {self._property} Decorator::set_value {self.value}")
        # Do PV domain have API to set value?
        return False

    def available(self):
        logger.debug(f" {self._property} Decorator::available")
        # TODO how to get this from application ?
        logger.debug(f"Using DECORATOR DOMAIN for {self._property}")

        try:
            res = {
                "show": self._decorator.canShowWidget(
                    AdvancedDecorator.panel_visibility_mode
                ),
                "enable": self._decorator.enableWidget(),
                "query": self._decorator.canShowWidget(
                    AdvancedDecorator.panel_visibility_mode
                ),
            }
            logger.debug(f"{res}")
        except Exception as ex:
            logger.error(f"ERROR setting {ex}")
        return res

    def valid(self, required_level=2):
        logger.debug(f" {self._property} Decorator::valid")
        if self._level < required_level:
            return True
        return True


# TODO see also  self._property.GetPanelVisibilityDefaultForRepresentation
class AdvancedDecorator(PropertyDomain):
    panel_visibility_mode = "default"

    def __init__(self, _proxy, _property, **kwargs):
        super().__init__(_proxy, _property, **kwargs)
        self._pv_proxy = _proxy.object
        self._pv_property = self._pv_proxy.GetProperty(_property)
        self._pv_name = kwargs.get("pv_name")
        self._level = 0
        self._property = _property
        self._decorator = None
        self._visibility = "default"

        self._visibility = self._pv_proxy.GetProperty(
            self._property
        ).GetPanelVisibility()

    def set_value(self):
        logger.debug(f" ADV {self._property} Decorator::set_value {self.value}")
        # Do PV domain have API to set value?
        return False

    def available(self):
        logger.debug(f"ADV {self._property} Decorator::available")
        logger.debug(
            f"ADV Using DECORATOR DOMAIN for {self._property} with visibility {self._visibility} domain visibility {self.panel_visibility_mode}"
        )
        show = False
        if self._visibility == "never":
            show = False
        elif self._visibility == "default":
            show = True
        elif (
            self._visibility == "advanced" and self.panel_visibility_mode == "advanced"
        ):
            show = True
        else:
            show = False

        try:
            res = {
                "show": show,
                "enable": True,
                "query": True,
            }
            logger.debug(f"{res}")
        except Exception as ex:
            logger.error(f"ERROR setting {ex}")
        return res

    def valid(self, required_level=2):
        logger.debug(f" {self._property} Decorator::valid")
        if self._level < required_level:
            return True
        return True


# -----------------------------------------------------------------------------


def register_domains():
    register_property_domain("ParaViewDomain", ParaViewDomain)
    register_property_domain("ParaViewDecoratorDomain", ParaViewDecoratorDomain)
    register_property_domain("AdvancedDecorator", AdvancedDecorator)
