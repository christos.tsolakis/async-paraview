# Build Instructions for trame example:

```
python3 -m venv ./venv
source ./venv/bin/activate
pip install -U pip
pip install trame
export PYTHONPATH=<buildDirectory>/lib/python<version>/site-packages

python <example_name.py>
```
