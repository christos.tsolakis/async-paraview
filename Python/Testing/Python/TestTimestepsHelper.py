import asyncio
from math import isclose


def list_is_close(a, b, tol):
    if len(a) != len(b):
        return False
    for x, y in zip(a, b):
        if not isclose(x, y, abs_tol=tol):
            return False
    return True


from async_paraview.services import ParaT, PipelineBuilder, PropertyManager


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    source = await builder.CreateProxy("sources", "TimeSource")

    await pmanager.UpdatePipeline(source)
    timesteps = pmanager.GetValues(source)["TimestepValues"]
    expected = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 1.0]

    assert list_is_close(timesteps, expected, tol=0.1)

    await App.close(session)


asyncio.run(main())
