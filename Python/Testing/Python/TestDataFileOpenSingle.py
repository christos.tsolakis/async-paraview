import asyncio

from async_paraview.services import ParaT, DataFile, PropertyManager
from vtkmodules.vtkCommonCore import VTK_IMAGE_DATA

from vtk.util.misc import vtkGetDataRoot
import os.path

expectedReaders =  [('sources', 'XMLImageDataReader')]

async def main():
    App = ParaT()
    session = await App.initialize()

    datafileService = DataFile(session)
    pm = PropertyManager()

    filename = os.path.join(vtkGetDataRoot(), "Testing/Data/rock.vti")
    possibleReaders = await datafileService.FindPossibleReaders(filename)
    assert possibleReaders == expectedReaders

    reader = await datafileService.Open(filename)
    assert reader is not None

    success = await pm.UpdatePipeline(reader, 0)
    assert success == True
    di = reader.GetDataInformation()

    await App.close(session)

    assert di.GetDataSetType() == VTK_IMAGE_DATA
    assert di.GetNumberOfPoints() == 17139400
    assert di.GetNumberOfCells() == 16917660



asyncio.run(main())
