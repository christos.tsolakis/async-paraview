import asyncio

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager
from async_paraview.modules.vtkRemotingPythonAsyncCore import (
    vtkPythonObservableWrapperUtilities,
)

from vtk.util.misc import vtkGetDataRoot
import os.path


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pm = PropertyManager()

    dataToLoad = os.path.join(vtkGetDataRoot(), "Testing/Data/EnSight/elements.case")

    # FIXME CreateProxy cannot figure out that ensight is a reader or handle CaseFileName yet
    # fall back to using C++ method
    reader = await vtkPythonObservableWrapperUtilities.GetFuture(
        builder.CreateReader("sources","ensight", dataToLoad)
    )

    await pm.UpdatePipeline(reader)

    info = reader.GetDataInformation()
    #print(info)
    assert info.GetNumberOfPoints() == 112
    assert info.GetNumberOfCells() == 16

    await App.close(session)


asyncio.run(main())
