import asyncio
import sys

from async_paraview.services import ParaT

""" This example demonstrates how to connect with a remote apvserver from python."""


async def main():
    app = ParaT()
    server_url = app.get_options().GetServerURL()
    session = await app.initialize(url=server_url)
    print(f"Connected to a remote server {server_url}")
    print(session)
    await asyncio.sleep(0.5)
    # Disconnect and quit remote server.
    await app.close(session, exit_server=True)


asyncio.run(main())
