import asyncio

from async_paraview.services import (
    ApplyController,
    ParaT,
    PipelineBuilder,
    PropertyManager,
    anext,
)


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    acontroller = ApplyController(session)
    pmanager = PropertyManager()

    sphere = await builder.CreateProxy(
        "sources", "SphereSource", ThetaResolution=80, Radius=2
    )
    shrink = await builder.CreateProxy(
        "filters", "ShrinkFilter", Input=sphere, ShrinkFactor=0.3
    )
    view = await builder.CreateProxy("views", "RenderView")

    acontroller.Apply()
    await anext(acontroller.GetObservable())

    # both proxies should have representations
    sphereRep = view.FindRepresentation(sphere, 0)
    shrinkRep = view.FindRepresentation(shrink, 0)

    assert sphereRep is not None
    assert shrinkRep is not None

    # sphere should be hidden since it is the input of shrink

    assert pmanager.GetValues(sphereRep)["Visibility"] == 0
    assert pmanager.GetValues(shrinkRep)["Visibility"] == 1

    # modify visibility manually and call Apply
    pmanager.SetValues(sphereRep, force_push=True, Visibility=1)
    acontroller.Apply()
    await anext(acontroller.GetObservable())

    # Apply should respect our choice

    assert pmanager.GetValues(sphereRep)["Visibility"] == 1
    assert pmanager.GetValues(shrinkRep)["Visibility"] == 1

    await App.close(session)


asyncio.run(main())
