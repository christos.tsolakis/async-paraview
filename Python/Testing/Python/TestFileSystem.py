import asyncio

from async_paraview.services import ParaT, FileSystem

# use python os for validation
import os


async def test_on_location(file_system, location):

    old_dir = os.path.join(os.getcwd(), "Old")
    new_dir = os.path.join(os.getcwd(), "New")

    success = await file_system.MakeDirectory(old_dir, location)
    assert success
    assert os.path.exists(old_dir)

    assert await file_system.Rename(old_dir, new_dir, location)
    assert success
    assert not os.path.exists(old_dir)
    assert os.path.exists(new_dir)

    # delete something that does not exist
    try:
        await file_system.Remove(old_dir, location)
        assert False
    except RuntimeError as error:
        print(str(error))

    # create dummy files to test Remove
    dummy1 = os.path.join(new_dir, "test_remove_python1")
    dummy2 = os.path.join(new_dir, "test_remove_python2")
    open(dummy1, "a")
    open(dummy2, "a")

    # remove a file
    assert await file_system.Remove(dummy1, location)
    # remove a file that does not exist raises an error
    try:
        await file_system.Remove(dummy1, location)
        assert False
    except RuntimeError as error:
        print(str(error))

    # remove a non-empty directory
    assert await file_system.Remove(new_dir, location)

    # Create and remove and empty directory
    assert await file_system.MakeDirectory(old_dir, location)

    assert await file_system.Remove(old_dir, location)

    # TODO add assertion test for format
    listing = await file_system.ListDirectory(os.getcwd(), FileSystem.Location.CLIENT)
    import json

    print(json.dumps(listing, indent=1))


async def main():
    App = ParaT()
    session = await App.initialize()

    file_system = FileSystem(session)
    await test_on_location(file_system, FileSystem.Location.CLIENT)
    await test_on_location(file_system, FileSystem.Location.SERVER)

    await App.close(session)


asyncio.run(main())
