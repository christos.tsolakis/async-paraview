import asyncio

from async_paraview.services import ParaT, PipelineBuilder, PropertyManager


async def main():
    App = ParaT()
    session = await App.initialize()

    builder = PipelineBuilder(session)
    pmanager = PropertyManager()

    ugrid = await builder.CreateProxy("sources", "FastUniformGrid")
    tracer = await builder.CreateProxy(
        "filters",
        "StreamTracer",
        Input=ugrid,
        SelectInputVectors=["", "", "", "0", "Swirl"],
        MaximumPropagation=20.0,
        IntegrationDirection=2,
    )

    pmanager.Push(tracer)

    # setup seeds line
    seeds = pmanager.GetValues(tracer)["Source"]
    pmanager.SetValues(
        seeds, Point1=[-10, -10, -10], Point2=[10, 10, 10], Resolution=1000
    )
    pmanager.Push(seeds)

    status = await pmanager.UpdatePipeline(tracer, 0)
    assert status
    print(f"{ugrid.GetDataInformation().GetNumberOfPoints()=}")
    assert ugrid.GetDataInformation().GetNumberOfPoints() == 9261
    print(f"{seeds.GetDataInformation().GetNumberOfPoints()=}")
    assert seeds.GetDataInformation().GetNumberOfPoints() == 1001
    print(f"{tracer.GetDataInformation().GetNumberOfPoints()=}")
    assert tracer.GetDataInformation().GetNumberOfPoints() == 754460

    await App.close(session)


asyncio.run(main())
